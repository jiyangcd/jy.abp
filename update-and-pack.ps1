# 设置错误时停止执行
$ErrorActionPreference = "Stop"

# 安装dotnet-outdated工具
Write-Host "正在检查dotnet-outdated工具..."
try {
    $outdatedVersion = dotnet tool list -g | Select-String "dotnet-outdated-tool"
    if (-not $outdatedVersion) {
        Write-Host "正在安装dotnet-outdated工具..."
        dotnet tool install --global dotnet-outdated-tool
    }
} catch {
    Write-Host "安装dotnet-outdated工具失败: $_"
    exit 1
}

# 获取所有的.csproj文件
$projects = Get-ChildItem -Path . -Filter *.csproj -Recurse

# 更新每个项目的NuGet包
Write-Host "正在更新NuGet包..."

# 定义一个函数来更新项目
function Update-Project {
    param(
        [string]$projectPath
    )
    Write-Host "更新项目: $projectPath"
    $maxRetries = 3
    $retryCount = 0
    $success = $false

    while (-not $success -and $retryCount -lt $maxRetries) {
        try {
            # 先分析依赖关系
            dotnet restore $projectPath
            # 使用 --highest-minor 参数来确保只更新次要版本和补丁版本
            dotnet outdated $projectPath --upgrade --highest-minor
            $success = $true
        } catch {
            $retryCount++
            if ($retryCount -lt $maxRetries) {
                Write-Host "更新失败，5秒后重试... (尝试 $retryCount/$maxRetries)"
                Start-Sleep -Seconds 5
            } else {
                Write-Host "更新失败: $_"
                return $false
            }
        }
    }
    return $true
}

# 首先更新框架项目
$frameworkProjects = $projects | Where-Object { $_.FullName -like "*\framework\src\*" }
Write-Host "正在更新框架项目..."
foreach ($project in $frameworkProjects) {
    if (-not (Update-Project $project.FullName)) {
        exit 1
    }
}

# 然后更新其他项目
$otherProjects = $projects | Where-Object { $_.FullName -notlike "*\framework\src\*" }
Write-Host "正在更新其他项目..."
foreach ($project in $otherProjects) {
    if (-not (Update-Project $project.FullName)) {
        exit 1
    }
}

# 编译解决方案
Write-Host "正在编译解决方案..."
$solutions = Get-ChildItem -Path . -Filter *.sln -Recurse
foreach ($solution in $solutions) {
    Write-Host "编译解决方案: $($solution.FullName)"
    dotnet build $solution.FullName -c Release
}

# 执行打包脚本
Write-Host "开始执行打包脚本..."
$packScript = Join-Path $PSScriptRoot "nupkg\pack.ps1"
if (Test-Path $packScript) {
    & $packScript
} else {
    Write-Error "找不到打包脚本: $packScript"
}

Write-Host "所有操作已完成"
# Paths
$packFolder = (Get-Item -Path "./" -Verbose).FullName
$rootFolder = Join-Path $packFolder "../"

function Write-Info   
{
	param(
        [Parameter(Mandatory = $true)]
        [string]
        $text
    )

	Write-Host $text -ForegroundColor Black -BackgroundColor Green

	try 
	{
	   $host.UI.RawUI.WindowTitle = $text
	}		
	catch 
	{
		#Changing window title is not suppoerted!
	}
}

function Write-Error   
{
	param(
        [Parameter(Mandatory = $true)]
        [string]
        $text
    )

	Write-Host $text -ForegroundColor Red -BackgroundColor Black 
}

function Seperator   
{
	Write-Host ("_" * 100)  -ForegroundColor gray 
}

function Get-Current-Version { 
	$commonPropsFilePath = resolve-path "../common.props"
	$commonPropsXmlCurrent = [xml](Get-Content $commonPropsFilePath ) 
	$currentVersion = $commonPropsXmlCurrent.Project.PropertyGroup.Version.Trim()
	return $currentVersion
}

function Get-Current-Branch {
	return git branch --show-current
}	   

function Read-File {
	param(
        [Parameter(Mandatory = $true)]
        [string]
        $filePath
    )
		
	$pathExists = Test-Path -Path $filePath -PathType Leaf
	if ($pathExists)
	{
		return Get-Content $filePath		
	}
	else{
		Write-Error  "$filePath path does not exist!"
	}
}

# List of solutions
$solutions = (
    "framework"
    #"modules/Jy.Abp.GeneralTree"
)

# List of projects
$projects = (

    # framework
    "framework/src/Jy.Utilities",
    "framework/src/Jy.Abp.Core",
    "framework/src/Jy.Abp.BlobStoring",
    "framework/src/Jy.Abp.Authorization",
    "framework/src/Jy.Abp.Localization",
    "framework/src/Jy.Abp.Settings",
    "framework/src/Jy.Abp.AspNetCore",
    "framework/src/Jy.Abp.AspNetCore.Serilog",
    "framework/src/Jy.Abp.Playwright",

    "framework/src/Jy.Abp.Application",
    "framework/src/Jy.Abp.Application.Contracts",
    "framework/src/Jy.Abp.Domain",
    "framework/src/Jy.Abp.Domain.Shared",

    # modules/extensions
    "modules/Jy.Abp.BlobStoring/Jy.Abp.BlobStoring.Application.Contracts",
    "modules/Jy.Abp.BlobStoring/Jy.Abp.BlobStoring.HttpApi",

    "modules/Jy.Abp.Identity/Jy.Abp.Identity.Application.Contracts",
    "modules/Jy.Abp.Identity/Jy.Abp.Identity.Application",
    "modules/Jy.Abp.Identity/Jy.Abp.Identity.HttpApi",

    # "modules/Jy.Abp.TypedSettings/Jy.Abp.TypedSettings.Domain",
    # "modules/Jy.Abp.TypedSettings/Jy.Abp.TypedSettings.Application",
    "modules/Jy.Abp.SettingManagement/Jy.Abp.SettingManagement.Domain",
    "modules/Jy.Abp.SettingManagement/Jy.Abp.SettingManagement.Application",
        
    # modules/Jy.Abp.GeneralTree
    "modules/Jy.Abp.GeneralTree/Jy.Abp.GeneralTree.Domain.Shared",
    "modules/Jy.Abp.GeneralTree/Jy.Abp.GeneralTree.Domain",
    "modules/Jy.Abp.GeneralTree/Jy.Abp.GeneralTree.Application.Contracts",
    "modules/Jy.Abp.GeneralTree/Jy.Abp.GeneralTree.Application",
    "modules/Jy.Abp.GeneralTree/Jy.Abp.GeneralTree.EntityFrameworkCore"
    #"modules/Jy.Abp.GeneralTree/Jy.Abp.GeneralTree.MongoDB"

)

. ".\common.ps1"

$apiKey = $args[0]

if ([string]::IsNullOrEmpty($apiKey)) {  
    #Write-Host "Using API key from environment variable: $apiKey"
	$apiKey = $env:NugetApiKey
} 

# Get the version
[xml]$commonPropsXml = Get-Content (Join-Path $rootFolder "common.props")
$version = $commonPropsXml.Project.PropertyGroup.Version

# Publish all packages
$i = 0
$errorCount = 0
$totalProjectsCount = $projects.length
$nugetUrl = "https://api.nuget.org/v3/index.json"
Set-Location $packFolder

# 获取目录中的所�?.nupkg 文件
$packages = Get-ChildItem -Path $packFolder -Filter *.nupkg -Recurse

foreach($package in $packages) {
	$i += 1
	#$projectFolder = Join-Path $rootFolder $project
	#$projectName = ($project -split '/')[-1]	
	$nugetPackageName = $package.FullName #$projectName + "." + $version + ".nupkg"	
	$nugetPackageExists = Test-Path $nugetPackageName -PathType leaf
 
	Write-Info "[$i / $totalProjectsCount] - Pushing: $nugetPackageName"
	
	if ($nugetPackageExists)
	{
		dotnet nuget push $nugetPackageName --skip-duplicate -s $nugetUrl --api-key "$apiKey"		
		#Write-Host ("Deleting package from local: " + $nugetPackageName)
		#Remove-Item $nugetPackageName -Force
	}
	else
	{
		Write-Host ("********** ERROR PACKAGE NOT FOUND: " + $nugetPackageName) -ForegroundColor red
		$errorCount += 1
		#Exit
	}
	
	Write-Host "--------------------------------------------------------------`r`n"
}

if ($errorCount > 0)
{
	Write-Host ("******* $errorCount error(s) occured *******") -ForegroundColor red
}

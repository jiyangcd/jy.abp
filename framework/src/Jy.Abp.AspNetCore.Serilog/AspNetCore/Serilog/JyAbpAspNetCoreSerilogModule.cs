﻿using Volo.Abp.AspNetCore.Serilog;
using Volo.Abp.Modularity;

namespace Jy.Abp.AspNetCore.Serilog;

[DependsOn(
    typeof(JyAbpAspNetCoreModule),
    typeof(AbpAspNetCoreSerilogModule))]
public class JyAbpAspNetCoreSerilogModule : AbpModule
{

}

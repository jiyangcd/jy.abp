﻿using System;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Jy.Abp.AspNetCore.Serilog;

public static class IHostBuilderExtensions
{
    public static IHostBuilder UseAbpSerilog(
        this IHostBuilder hostBuilder,
        Action<LoggerConfiguration> configurationAction = null,
        bool setDefault = true
        )
    {
        return hostBuilder.UseSerilog((context, services, configuration) =>
        {
            if (setDefault)
                configuration
                 .ReadFrom.Configuration(context.Configuration)
                 .ReadFrom.Services(services)
                 .Enrich.FromLogContext();

            configurationAction?.Invoke(configuration);
        });
    }
}

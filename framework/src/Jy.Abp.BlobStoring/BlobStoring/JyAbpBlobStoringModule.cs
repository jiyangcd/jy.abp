﻿using Volo.Abp.BlobStoring;
using Volo.Abp.Modularity;

namespace Jy.Abp.BlobStoring;

[DependsOn(typeof(AbpBlobStoringModule))]
public class JyAbpBlobStoringModule : AbpModule
{

}

﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;
using Volo.Abp.Validation;

namespace Jy.Abp.Ddd.Domain.Shared;

[DependsOn(typeof(AbpValidationModule), typeof(AbpDddDomainSharedModule))]
public class JyAbpDomainSharedModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //Configure<AbpVirtualFileSystemOptions>(options =>
        //{
        //    options.FileSets.AddEmbedded<JyAbpDomainSharedModule>();
        //});
    }
}

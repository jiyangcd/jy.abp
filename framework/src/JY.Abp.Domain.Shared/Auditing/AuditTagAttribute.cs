﻿using System;

namespace Jy.Abp.Auditing;

public class AuditTagAttribute : Attribute
{
    public string Tag { get; set; }

    public AuditTagAttribute(string tag)
    {
        Tag = tag;
    }
}

﻿using Volo.Abp.BlobStoring;

namespace Jy.Abp.BlobStoring;

public class BlobContainerCacheItem
{
    public BlobContainerCacheItem(string name, BlobContainerConfiguration configuration)
    {
        Name = name;
        Configuration = configuration;
    }

    public string Name { get; private set; }

    public BlobContainerConfiguration Configuration { get; private set; }
}

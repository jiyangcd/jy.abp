﻿namespace Jy.Abp.Application;

public class KeywordsPagedAndSortedFilter : PagedAndSortedFilter
{
    public string Keywords { get; set; }
}


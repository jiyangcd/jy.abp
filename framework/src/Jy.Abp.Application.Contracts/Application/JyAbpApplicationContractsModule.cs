﻿using Jy.Abp.Authorization;
using Jy.Abp.Ddd.Domain.Shared;
using Volo.Abp.Application;
using Volo.Abp.Modularity;

namespace Jy.Abp.Application;

[DependsOn(
    typeof(JyAbpDomainSharedModule),
    typeof(JyAbpAuthorizationModule),

    typeof(AbpDddApplicationContractsModule)
    )]
public class JyAbpApplicationContractsModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        //context.Services.OnRegistered();
    }
}

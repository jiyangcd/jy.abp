using System;
using Jy.Abp.Authorization;
using Microsoft.AspNetCore.Authorization;

namespace Jy.Abp.Application;

public class AppAuthorizeAttribute : AuthorizeAttribute
{
    /// <summary>
    /// permissions可传入任意的枚举及字符串进行验证
    /// 将使用<see cref="PermissionHelper.GetCombination(object[])"/>获取权限名称（如果是多个权限将生成权限组合）
    /// </summary>
    public AppAuthorizeAttribute(params object[] permissions)
        : base(PermissionHelper.GetCombination(permissions))
    {

    }
}

public class AppAuthorizeAttribute<TEnum> : AuthorizeAttribute
    where TEnum : Enum
{
    public AppAuthorizeAttribute()
        : base(PermissionHelper.GetPermissionName<TEnum>())
    {

    }
}



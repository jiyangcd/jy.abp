﻿using System;
using Volo.Abp.Application.Dtos;

namespace Jy.Abp.Application;


public class PagedAndSortedFilter : PagedAndSortedResultRequestDto
{
    public const int DefaultSize = 20;

    public void SetDefaultSortIfNot(string field, SortDirection sortDirection = SortDirection.Desc)
    {
        if (Sorting.HasValue())
            return;
        Sorting = $"{field} {sortDirection.ToString().ToLower()}";
    }

}


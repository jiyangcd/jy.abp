﻿using Microsoft.AspNetCore.Cors.Infrastructure;

namespace Jy.Abp.AspNetCore.Cors;

public static class CorsOptionsExtensions
{
    /// <summary>
    /// 默认配置
    /// </summary>
    public static void ConfigureDefault(
        this CorsOptions options, 
        string[] origins) 
    {
        options.AddDefaultPolicy(
                    builder => builder
                        .WithOrigins(origins)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                );
    }
}

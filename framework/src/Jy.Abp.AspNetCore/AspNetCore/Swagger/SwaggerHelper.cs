using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Jy.Abp.Services;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.IdentityModel.Tokens;

namespace Jy.Abp.AspNetCore.Swagger;

public static class SwaggerHelper
{
    private static Dictionary<string, IEnumerable<ServieGroupAttribute>> _actionGroupAttrs = new Dictionary<string, IEnumerable<ServieGroupAttribute>>();

    public static string DefaultDocName = "default";
    public static string ApplicationName = "web";
    public static Dictionary<string, string> Groups = new Dictionary<string, string>();

    /// <summary>
    /// 获取接口对应的验证attr
    /// </summary>
    private static IEnumerable<ServieGroupAttribute> GetGroupAttrs(ControllerActionDescriptor actionDescriptor)
    {
        if (!_actionGroupAttrs.ContainsKey(actionDescriptor.DisplayName))
        {
            var actAttrs = actionDescriptor.MethodInfo.GetCustomAttributes()
                .Where(t => t is ServieGroupAttribute)
                .Select(t => t as ServieGroupAttribute);

            if (actAttrs != null && actAttrs.Any())
            {
                actAttrs = actionDescriptor.ControllerTypeInfo.GetCustomAttributes()
                   .Where(t => t is ServieGroupAttribute)
                   .Select(t => t as ServieGroupAttribute);
            }
            _actionGroupAttrs[actionDescriptor.DisplayName] = actAttrs;
        }
        return _actionGroupAttrs[actionDescriptor.DisplayName];
    }

    public static void Init(string applicationName)
    {
        ApplicationName = applicationName;
        Groups.Add(DefaultDocName, $"{ApplicationName} API");
        foreach (var pt in ServieGroupAttribute.Groups)
        {
            Groups.Add($"{pt}", $"{pt} API");
        }
    }

    /// <summary>
    /// 检测当前doc是否有显示此action api的权限
    /// </summary>
    public static bool CheckGroup(
        string docName,
        ControllerActionDescriptor action)
    {
        var attrs = GetGroupAttrs(action);
        if (attrs.Any() && docName != DefaultDocName)
            return attrs.Any(t => t.Group == docName);

        return true;
    }
}


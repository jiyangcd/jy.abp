﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Jy.Abp.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.RequestLocalization;
using Volo.Abp.AspNetCore;
using Volo.Abp.Modularity;
using Volo.Abp.Swashbuckle;

namespace Jy.Abp.AspNetCore;

[DependsOn(
    typeof(JyAbpCoreModule),
    typeof(AbpAspNetCoreModule),
    typeof(AbpSwashbuckleModule)
    )]
public class JyAbpAspNetCoreModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //abp本地化请求配置
        this.Configure<AbpRequestLocalizationOptions>(options =>
        {
            options.RequestLocalizationOptionConfigurators.Add((sp, options) =>
            {
                options.RequestCultureProviders
                        .ReplaceOne(t => t.GetType() == typeof(AcceptLanguageHeaderRequestCultureProvider), new DefaultAcceptLanguageHeaderRequestCultureProvider(options));
                return Task.CompletedTask;
            });
        });
    }
}

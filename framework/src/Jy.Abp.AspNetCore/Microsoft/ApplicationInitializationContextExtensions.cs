﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace Volo.Abp;

public static class ApplicationInitializationContextExtensions
{
    /// <summary>
    /// 获取ApplicationName使用<see cref="IApplicationInfoAccessor.ApplicationName"/>
    /// </summary>
    public static string GetApplicationName(this ApplicationInitializationContext context)
    {
        return context.ServiceProvider
            .GetRequiredService<IApplicationInfoAccessor>()
            .ApplicationName;
    }

    /// <summary>
    /// 获取AppName使用<see cref="IBrandingProvider.AppName"/>
    /// </summary>
    public static string GetAppName(this ApplicationInitializationContext context)
    {
        return context.ServiceProvider
            .GetRequiredService<IBrandingProvider>()
            .AppName;
    }
}

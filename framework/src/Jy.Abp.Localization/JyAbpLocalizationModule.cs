﻿using Volo.Abp.Localization;
using Volo.Abp.Modularity;

namespace Jy.Abp.Localization;

[DependsOn(typeof(AbpLocalizationModule))]
public class JyAbpLocalizationModule:AbpModule
{

}

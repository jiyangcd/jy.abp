﻿using System;

namespace Jy.Abp.Localization;

/// <summary>
/// 资源文件type提供者，用于获取相关类型对应的资源ResourceType
/// </summary>
public interface IResourceTypeProvider
{
    /// <summary>
    /// 获取资源类型
    /// </summary>
    Type GetResourceType(Type objectType);
}



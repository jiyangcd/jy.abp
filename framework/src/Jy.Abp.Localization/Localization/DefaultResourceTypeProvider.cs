﻿using System;
using System.Linq;
using Microsoft.Extensions.Options;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Localization.ExceptionHandling;

namespace Jy.Abp.Localization;

/// <summary>
/// 默认资源类型获取，从AbpExceptionLocalizationOptions配置中获取
/// </summary>
public class DefaultResourceTypeProvider : IResourceTypeProvider, ISingletonDependency
{
    private readonly AbpExceptionLocalizationOptions _abpExceptionLocalizationOptions;

    public DefaultResourceTypeProvider(IOptions<AbpExceptionLocalizationOptions> abpExceptionLocalizationOptions)
    {
        _abpExceptionLocalizationOptions = abpExceptionLocalizationOptions.Value;
    }

    public Type GetResourceType(Type objectType)
    {
        return _abpExceptionLocalizationOptions
            .ErrorCodeNamespaceMappings
            .FirstOrDefault(t => objectType.Namespace.StartsWith(t.Key))
            .Value;
    }
}



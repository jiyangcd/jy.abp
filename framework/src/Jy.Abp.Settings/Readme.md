﻿# Jy.Abp.Settings

一个基于ABP框架的类型化配置管理模块，提供强类型的配置定义和访问能力。

## 功能特点

- 支持强类型的配置定义
- 自动处理配置值的类型转换和序列化
- 支持默认值设置
- 支持配置本地化
- 支持多种配置提供者（全局、用户等）
- 支持复杂对象和数组类型的配置

## 使用说明

### 1. 安装

```bash
dotnet add package Jy.Abp.Settings
```

### 2. 模块配置

在你的模块类中添加依赖：

```csharp
[DependsOn(typeof(JyAbpSettingsModule))]
public class YourModule : AbpModule
{
}
```

### 3. 定义配置类

创建一个继承自`TypedSettingsBase`的配置类：

```csharp
public class SystemSettings : TypedSettingsBase
{
    // 使用属性默认值作为配置默认值
    public virtual string Name { get; set; } = "SimpleApp";

    // 使用TypedSettingDefinition特性自定义默认值
    [TypedSettingDefinition("MyDesc")]
    public virtual string Description { get; set; } = "";

    // 支持枚举类型
    [TypedSettingDefinition(SystemSettingsType.Test1)]
    public virtual SystemSettingsType Type { get; set; }

    // 支持基础值类型
    [TypedSettingDefinition(true)]
    public virtual bool BoolSetting { get; set; }

    [TypedSettingDefinition(1.0)]
    public virtual decimal DecimalSetting { get; set; }
}

public enum SystemSettingsType 
{
    Test1,
    Test2,
    Test3
}
```

### 4. 复杂类型配置

除了基础类型外，还支持复杂对象和数组类型的配置：

```csharp
public class SystemSettings : TypedSettingsBase
{
    // 复杂对象类型
    public virtual SystemSettingsItem Item { get; set; } = new SystemSettingsItem() { 
        Id = 1,
        Name = "item1"
    };

    // 可空的复杂对象
    public virtual SystemSettingsItem Item1 { get; set; }

    // 数组类型
    public virtual SystemSettingsItem[] Items { get; set; }
        = new[] { 
            new SystemSettingsItem() { Id = 2, Name = "item2" },
            new SystemSettingsItem() { Id = 3, Name = "item3" }
        };
}

public class SystemSettingsItem 
{
    public int Id { get; set; }
    public string Name { get; set; }
}
```

### 5. 配置特性说明

`TypedSettingDefinition`特性提供了以下配置选项：

```csharp
[TypedSettingDefinition(
    defaultValue: "default",           // 默认值
    isVisibleToClients: true,         // 是否对客户端可见
    isInherited: true,                // 是否从父级继承
    isEncrypted: false,               // 是否加密存储
    providers: new[] { "G", "U" }     // 指定配置提供者
)]
public virtual string Setting { get; set; }
```

### 6. 使用配置

直接注入配置类即可使用：

```csharp
public class YourService
{
    private readonly SystemSettings _settings;

    public YourService(SystemSettings settings)
    {
        _settings = settings;
    }

    public async Task DoSomething()
    {
        var name = _settings.Name;                 // 获取配置值
        var item = _settings.Item;                 // 获取复杂对象配置
        var items = _settings.Items;               // 获取数组配置
    }
}
```

### 7. 配置本地化

配置名称和描述的本地化资源键：
- 显示名称：`Settings.{命名空间第一段}.{类名去掉Settings}.{属性名}.DisplayName`
- 描述：`Settings.{命名空间第一段}.{类名去掉Settings}.{属性名}.Description`

例如：
```json
{
  "culture": "zh-Hans",
  "texts": {
    "Settings.SimpleApp.System.Name.DisplayName": "系统名称",
    "Settings.SimpleApp.System.Name.Description": "系统的显示名称"
  }
}
```

## 最佳实践

1. 配置分组：将相关的配置项组织在同一个配置类中
2. 合理使用默认值：为所有配置项设置合适的默认值
3. 复杂对象处理：
   - 复杂类型会被自动序列化为JSON存储
   - 复杂对象应该是简单的POCO类，避免包含复杂逻辑
   - 建议将大型配置拆分为多个小的配置类
4. 配置本地化：
   - 为所有配置项提供本地化的显示名称和描述
   - 使用统一的本地化资源管理
5. 配置提供者：
   - 根据实际需求选择合适的配置提供者
   - 可以通过特性指定配置项使用的提供者

## 注意事项

1. 所有配置属性必须定义为virtual
2. 配置类必须继承TypedSettingsBase
3. 配置值会自动进行类型转换和序列化处理
4. 配置的命名遵循特定规则：{命名空间第一段}.{类名去掉Settings}.{属性名}
5. 建议在应用启动时预加载常用配置，避免频繁读取

﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Jy.Utilities;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.DynamicProxy;
using Volo.Abp.Settings;

namespace Jy.Abp.Settings;

public class TypedSettingsInterceptor : AbpInterceptor, ITransientDependency
{
    public const string GetMethodPrev = "get_";
    public const string SetMethodPrev = "set_";

    private readonly ISettingProvider _settingProvider;
    private readonly ITypedSettingsValueSerializer _valueSerializer;

    public TypedSettingsInterceptor(ISettingProvider settingProvider, ITypedSettingsValueSerializer valueSerializer)
    {
        _settingProvider = settingProvider;
        _valueSerializer = valueSerializer;
    }

    /// <summary>
    /// 通过属性方法获取属性的SettingName
    /// </summary>
    private string GetPropertySettingName(MethodInfo method)
    {
        var pname = method.Name.Replace(GetMethodPrev, "")
                               .Replace(SetMethodPrev, "");
        return TypedSettingsHelper.CreateSettingName(method.DeclaringType, pname);
    }

    private async Task<object> GetPropertySettingValueAsync(MethodInfo propertyGetMethod) 
    {
        try
        {
            var settingName = this.GetPropertySettingName(propertyGetMethod);
            var settingValue = await this._settingProvider.GetOrNullAsync(settingName);
            var returnType = propertyGetMethod.ReturnType;
            return _valueSerializer.Deserialize(returnType, settingValue);
        }
        catch (Exception ex)
        {
            throw new AbpException($"无法将配置值转换为{propertyGetMethod.ReturnType.Name}类型", ex);
        }
    }

    public override async Task InterceptAsync(IAbpMethodInvocation invocation)
    {
        //获取属性值，直接返回配置数据
        if (invocation.Method.Name.StartsWith(GetMethodPrev))
        {
            invocation.ReturnValue = await GetPropertySettingValueAsync(invocation.Method);
            return;

        }
        await invocation.ProceedAsync();
    }
}

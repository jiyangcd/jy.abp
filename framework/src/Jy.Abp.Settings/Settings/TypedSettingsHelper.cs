using System;
using System.Collections.Generic;
using System.Reflection;
using Volo.Abp.Settings;

namespace Jy.Abp.Settings;

public static class TypedSettingsHelper
{
    public static string CreateSettingName(Type type, string propertyName)
    {
        return $"{type.Namespace.Split(".")[0]}.{type.Name.RemovePostFix("Settings")}.{propertyName}";
    }

    public static string CreateSettingName(PropertyInfo property)
    {
        return CreateSettingName(property.ReflectedType, property.Name);
    }

    public static object GetDefaultValue(Type targetType)
    {
        return targetType.IsValueType ? Activator.CreateInstance(targetType) : null;
    }

    public static bool IsSupportProvider(this SettingDefinition setting, string provider)
    {
        if (setting.Providers.IsNullOrEmpty())
            return true;
        return setting.Providers.Contains(provider);
    }

    public static T CreateInstance<T>()
        where T : ITypedSettings
    {
        return Activator.CreateInstance<T>();
    }
}


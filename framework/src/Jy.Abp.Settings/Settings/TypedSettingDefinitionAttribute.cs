using System;
using System.Collections.Generic;
using Volo.Abp.Settings;

namespace Jy.Abp.Settings;

[AttributeUsage(AttributeTargets.Property)]
public class TypedSettingDefinitionAttribute : Attribute
{
    /// <summary>
    /// 默认值 
    /// </summary>
    public object DefaultValue { get; set; }
    /// <summary>
    /// 是否客户端显示
    /// </summary>
    public bool IsVisibleToClients { get; set; } = true;
    /// <summary>
    /// 如果指定，则只使用指定的provider获取数据
    /// </summary>
    public string[] Providers { get; set; }
    /// <summary>
    /// Is this setting inherited from parent scopes. Default: True.
    /// </summary>
    public bool IsInherited { get; set; } = true;
    /// <summary>
    /// Can be used to get/set custom properties for this setting definition.
    /// </summary>
    public Dictionary<string, object> Properties { get; }
    /// <summary>
    /// 是否加密. Default: False
    /// </summary>
    public bool IsEncrypted { get; set; }

    public TypedSettingDefinitionAttribute(
        object defaultValue,
        params string[] providers
        )
    {
        DefaultValue = defaultValue;
        Providers = providers;
    }
}



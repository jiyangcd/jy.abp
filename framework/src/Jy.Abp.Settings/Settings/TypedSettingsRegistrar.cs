﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using Volo.Abp.DependencyInjection;

namespace Jy.Abp.Settings;

public static class TypedSettingsRegistrar
{
    public static List<Type> Types { get; } = new List<Type>();

    public static void RegisterIfNeed(IOnServiceRegistredContext context)
    {
        if (typeof(ITypedSettings).GetTypeInfo().IsAssignableFrom(context.ImplementationType))
        {
            context.Interceptors.TryAdd<TypedSettingsInterceptor>();
            Types.Add(context.ImplementationType);
        }
    }
}

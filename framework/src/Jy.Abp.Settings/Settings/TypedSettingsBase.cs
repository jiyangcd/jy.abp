using Volo.Abp.DependencyInjection;

namespace Jy.Abp.Settings;

/// <summary>
/// 类型化配置基类
/// SettingName：{命名空间第一段}.{类名去掉Settings}.{属性名}
/// LocalizableName：Settings.{name}.DisplayName/Settings.{name}.Description
/// 默认值为字段的默认值.ToString()
/// 可以使用SettingDefinitionAttribute重新定义配置的相关数据
/// 所有属性需要定义为virtual.
/// 属性只支持string、枚举以及其它值类型
/// </summary>
[ExposeServices(typeof(ITypedSettings), IncludeSelf = true)]
public abstract class TypedSettingsBase : ITypedSettings
{
    public TypedSettingsBase()
    {

    }
}


using System;
using System.Collections.Generic;
using System.Reflection;
using Jy.Abp.Localization;
using Volo.Abp.Localization;
using Volo.Abp.Settings;

namespace Jy.Abp.Settings;

/// <summary>
/// 自动配置定义
/// </summary>
public class TypedSettingsDefinitionProvider : SettingDefinitionProvider
{
    private readonly IResourceTypeProvider _resourceTypeProvider;
    private readonly ITypedSettingsValueSerializer _typedSettingsValueSerializer;

    public TypedSettingsDefinitionProvider(
        IResourceTypeProvider resourceTypeProvider,
        ITypedSettingsValueSerializer typedSettingsValueSerializer)
    {
        _resourceTypeProvider = resourceTypeProvider;
        _typedSettingsValueSerializer = typedSettingsValueSerializer;
    }

    public override void Define(ISettingDefinitionContext context)
    {
        foreach (var type in TypedSettingsRegistrar.Types)
        {
            var resourceType = _resourceTypeProvider.GetResourceType(type);
            var settings = Activator.CreateInstance(type);
            foreach (var p in type.GetProperties())
            {
                var attr = p.GetCustomAttribute<TypedSettingDefinitionAttribute>();
                var name = TypedSettingsHelper.CreateSettingName(p);
                var defaultValue = attr?.DefaultValue ?? p.GetValue(settings);
                var definition = new SettingDefinition(
                       name: name,
                       defaultValue: _typedSettingsValueSerializer.Serialize(defaultValue),
                       displayName: LocalizableString.Create(resourceType, $"Settings.{name}.DisplayName"),
                       description: LocalizableString.Create(resourceType, $"Settings.{name}.Description"),
                       isVisibleToClients: attr?.IsVisibleToClients ?? true
                        );
                if (attr != null)
                {
                    definition.IsEncrypted = attr.IsEncrypted;
                    definition.IsInherited = attr.IsInherited;
                    if (!attr.Properties.IsNullOrEmpty())
                        foreach (var ap in attr.Properties)
                            definition.WithProperty(ap.Key, ap.Value);

                    definition.WithProviders(attr.Providers);
                }
                context.Add(definition);
            }
        }
    }
}



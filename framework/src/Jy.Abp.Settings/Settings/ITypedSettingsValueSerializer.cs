using System;

namespace Jy.Abp.Settings;

public interface ITypedSettingsValueSerializer
{
    /// <summary>
    /// 将值序列化为字符串
    /// </summary>
    string Serialize(object value);

    /// <summary>
    /// 将字符串反序列化为指定类型的值
    /// </summary>
    object Deserialize(Type type, string value);
}
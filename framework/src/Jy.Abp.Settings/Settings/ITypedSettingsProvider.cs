﻿using System.Threading.Tasks;

namespace Jy.Abp.Settings;

public interface ITypedSettingsProvider
{
    /// <summary>
    /// 获取类型化配置值
    /// </summary>
    Task<TSettings> GetAsync<TSettings>() where TSettings : ITypedSettings;
}
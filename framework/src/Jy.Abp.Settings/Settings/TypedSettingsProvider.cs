﻿using System.Threading.Tasks;
using Jy.Utilities;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Settings;

namespace Jy.Abp.Settings;

public class TypedSettingsProvider : ITypedSettingsProvider, ISingletonDependency
{
    /// <summary>
    /// 全局配置缓存
    /// </summary>
    private readonly ISettingProvider _settingProvider;
    private readonly ITypedSettingsValueSerializer _valueSerializer;

    public TypedSettingsProvider(
        ISettingProvider settingProvider,
        ITypedSettingsValueSerializer valueSerializer)
    {
        _settingProvider = settingProvider;
        _valueSerializer = valueSerializer;
    }

    public async Task<TSettings> GetAsync<TSettings>()
        where TSettings : ITypedSettings
    {
        var settings = TypedSettingsHelper.CreateInstance<TSettings>();
        var ps = typeof(TSettings).GetProperties();
        foreach (var p in ps)
        {
            var settingName = TypedSettingsHelper.CreateSettingName(p);
            var settingVal = await this._settingProvider.GetOrNullAsync(settingName);
            var propertyVal = _valueSerializer.Deserialize(p.PropertyType, settingVal);
            p.SetValue(settings, propertyVal);
        }
        return settings;
    }
}


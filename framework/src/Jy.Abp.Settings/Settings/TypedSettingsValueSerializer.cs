using System;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Json;

namespace Jy.Abp.Settings;

public class TypedSettingsValueSerializer : ITypedSettingsValueSerializer, ITransientDependency
{
    private readonly IJsonSerializer _jsonSerializer;

    public TypedSettingsValueSerializer(IJsonSerializer jsonSerializer)
    {
        _jsonSerializer = jsonSerializer;
    }

    public virtual string Serialize(object value)
    {
        try
        {
            if (value == null)
                return null;

            var type = value.GetType();
            if (type.IsPrimitive || type == typeof(string))
                return value.ToString();

            if (type.IsEnum)
                return value.ToString();

            return _jsonSerializer.Serialize(value);
        }
        catch (Exception ex)
        {
            throw new Exception($"序列化设置值时发生错误: {ex.Message}", ex);
        }
    }

    public virtual object Deserialize(Type type, string value)
    {
        try
        {
            if (value == null)
                return null;

            // 处理可空类型
            var nullableType = Nullable.GetUnderlyingType(type);
            if (nullableType != null)
            {
                if (string.IsNullOrEmpty(value))
                    return null;
                type = nullableType;
            }

            if (type.IsPrimitive || type == typeof(string))
                return Convert.ChangeType(value, type);

            if (type.IsEnum)
                return Enum.Parse(type, value);

            return _jsonSerializer.Deserialize(type, value);
        }
        catch (Exception ex)
        {
            throw new Exception($"反序列化设置值时发生错误: {ex.Message}", ex);
        }
    }
}
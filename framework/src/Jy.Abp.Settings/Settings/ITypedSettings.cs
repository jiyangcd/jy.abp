﻿using Volo.Abp.DependencyInjection;

namespace Jy.Abp.Settings;

/// <summary>
/// 类型化配置接口，获取时直接注入使用
/// </summary>
public interface ITypedSettings : ISingletonDependency
{
    
}
﻿using Jy.Abp.Localization;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Json;
using Volo.Abp.Modularity;

namespace Jy.Abp.Settings;

[DependsOn(typeof(JyAbpLocalizationModule), typeof(AbpJsonModule))]
public class JyAbpSettingsModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.OnRegistered(TypedSettingsRegistrar.RegisterIfNeed);
    }
}

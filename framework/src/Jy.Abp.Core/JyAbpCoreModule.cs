﻿using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Modularity;
using Jy.Abp.Enums;

namespace Jy.Abp;

public class JyAbpCoreModule : AbpModule
{
    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var moduleAssemblies = context.ServiceProvider
            .GetRequiredService<IModuleContainer>()
            .Modules
            .Select(t => t.Assembly)
            .ToArray();
        EnumHelper.RegisterAssemblies(moduleAssemblies);
        base.OnApplicationInitialization(context);
    }
}

﻿using System;
using System.Collections.Generic;

namespace Jy.Abp.Services;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class ServieGroupAttribute : Attribute
{
    public static List<string> Groups = new List<string>();

    public string Group { get; set; }

    public ServieGroupAttribute(string group)
    {
        Group = group;
        Groups.AddIfNotContains(group);
    }
}




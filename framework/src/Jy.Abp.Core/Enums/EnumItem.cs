﻿namespace Jy.Abp.Enums;

public class EnumItem
{
    public string Name { get; set; }
    public int Value { get; set; }
    public string Desc { get; set; }
}

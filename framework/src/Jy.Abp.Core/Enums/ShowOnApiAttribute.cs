﻿using System;

namespace Jy.Abp.Enums;

/// <summary>
/// 定义枚举是否在获取枚举的API上返回
/// </summary>
[AttributeUsage(AttributeTargets.Enum)]
public class ShowOnApiAttribute : Attribute
{

}


﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Jy.Utilities;

public static class EncryptHelper
{
    /// <summary>
    /// 32位MD5加密
    /// </summary>
    public static string Md5(string source)
    {
        var sb = new StringBuilder(32);
        var md5 = MD5.Create();
        var t = md5.ComputeHash(Encoding.Default.GetBytes(source));
        for (var i = 0; i < t.Length; i++)
        {
            sb.Append(t[i].ToString("x").PadLeft(2, '0'));
        }
        return sb.ToString();
    }
    /// <summary>
    /// SHA256
    /// </summary>
    public static string Sha256(string data)
    {
        var bytes = Encoding.UTF8.GetBytes(data);
        var hash = SHA256.Create().ComputeHash(bytes);

        var builder = new StringBuilder();
        for (var i = 0; i < hash.Length; i++)
        {
            builder.Append(hash[i].ToString("x2"));
        }

        return builder.ToString();
    }
    /// <summary>
    /// DES加密字符串
    /// </summary>
    /// <param name="content">待加密的字符串</param>
    /// <param name="key">加密密钥,要求为8位</param>
    /// <param name="iv">如果不指定，使用key</param>
    /// <param name="model">model</param>
    /// <param name="padding">padding</param>
    /// <returns>加密成功返回加密后的字符串，失败返回源串</returns>
    public static string DesEncrypt(
        string content,
        string key,
        string iv = null,
        CipherMode model = CipherMode.CBC,
        PaddingMode padding = PaddingMode.PKCS7)
    {
        try
        {
            var inputByteArray = Encoding.Default.GetBytes(content);

            var des = DES.Create();
            des.Mode = model;
            des.Padding = padding;
            des.Key = Encoding.ASCII.GetBytes(key);
            des.IV = Encoding.ASCII.GetBytes(iv ?? key);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Convert.ToBase64String(ms.ToArray());
        }
        catch
        {
            return content;
        }
    }
    /// <summary>
    /// DES解密字符串
    /// </summary>
    /// <param name="encryptContent">待解密的字符串</param>
    /// <param name="key">解密密钥,要求为8位,和加密密钥相同</param>
    /// <param name="iv"></param>
    /// <param name="model"></param>
    /// <param name="padding"></param>
    /// <returns>解密成功返回解密后的字符串，失败返源串</returns>
    public static string DesDecrypt(
        string encryptContent, 
        string key,
        string iv = null,
        CipherMode model = CipherMode.CBC,
        PaddingMode padding = PaddingMode.PKCS7)
    {
        var ms = new MemoryStream();
        try
        {
            var inputByteArray = Convert.FromBase64String(encryptContent);

            var des = DES.Create();
            des.Mode = model;
            des.Padding = padding;
            des.Key = Encoding.ASCII.GetBytes(key);
            des.IV = Encoding.ASCII.GetBytes(iv ?? key);
            var cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Encoding.Default.GetString(ms.ToArray());
        }
        catch
        {
            return encryptContent;
        }
    }
}

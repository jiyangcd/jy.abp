﻿using System;
using System.Threading;

namespace Jy.Utilities;
/// <summary>
/// 数据转换帮助类
/// </summary>
public static class ConvertHelper
{
    /// <summary>
    /// 数据转换，支持所有可以相互转换的类型
    /// 额外支持enum
    /// </summary>
    /// <param name="obj">对像</param>
    /// <param name="destType">目标类型</param>
    /// <returns>目标类型对像</returns>
    public static object ChangeType(object obj, Type destType)
    {
        return ChangeType(obj, destType, Thread.CurrentThread.CurrentCulture);
    }

    public static object ChangeType(object obj, Type destType, IFormatProvider provider)
    {
        // 如果目标类型是可空类型，获取其底层类型
        var underlyingType = Nullable.GetUnderlyingType(destType);
        destType = underlyingType ?? destType;

        // 对空值的处理
        if (obj == null)
        {
            // 如果目标类型允许为null（即为Nullable类型），直接返回null
            return (underlyingType != null) ? null : throw new FormatException("obj cannot be null for non-nullable type.");
        }

        // 处理枚举类型转换
        if (typeof(Enum).IsAssignableFrom(destType))
        {
            return Enum.Parse(destType, obj.ToString());
        }

        // 使用Convert.ChangeType进行常规类型转换
        return Convert.ChangeType(obj, destType, provider);
    }
}


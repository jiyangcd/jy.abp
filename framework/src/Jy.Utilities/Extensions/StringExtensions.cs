﻿namespace System;

public static class StringExtensions
{
    public static bool HasValue(this string content)
    {
        return !string.IsNullOrWhiteSpace(content);
    }
}


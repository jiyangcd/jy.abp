﻿using Jy.Abp.Ddd.Domain;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace Jy.Abp.Application;

[DependsOn(
    typeof(JyAbpDomainModule),
    typeof(JyAbpApplicationContractsModule),

    typeof(AbpDddApplicationModule),
    typeof(AbpAutoMapperModule)
)]
public class JyAbpApplicationModule : AbpModule
{

}

﻿using Jy.Abp.Ddd.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Swashbuckle;

namespace Jy.Abp.HttpApi;

[DependsOn(
    typeof(JyAbpApplicationContractsModule),
    typeof(AbpSwashbuckleModule))]
public class JyAbpHttpApiModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
     
    }
}

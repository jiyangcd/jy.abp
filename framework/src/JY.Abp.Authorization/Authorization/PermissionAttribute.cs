using System;
using Volo.Abp.MultiTenancy;

namespace Jy.Abp.Authorization;


/// <summary>
/// 用于指定枚举为系统权限，指定的类型会自动注册到权限系统
/// 如果是自定义验证逻辑，则不需要添加此特性
/// </summary>
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum)]
public class PermissionAttribute : PermissionGroupAttribute
{
    public const string LocalizablePrev = "Permission:";
    public const MultiTenancySides DefaultTenancySides = MultiTenancySides.Both;

    public PermissionAttribute(
        string displayName = null,
        MultiTenancySides multiTenancySides = DefaultTenancySides)
        : base(displayName)
    {
        MultiTenancySides = multiTenancySides;
    }
    /// <summary>
    /// 权限的租户类型
    /// </summary>
    public MultiTenancySides MultiTenancySides { get; set; }
}


﻿using System;
using Volo.Abp.Localization;

namespace Jy.Abp.Authorization;

/// <summary>
/// 所有定义权限
/// </summary>
[AttributeUsage(AttributeTargets.Enum | AttributeTargets.Class)]
public class PermissionGroupAttribute : Attribute
{
    /// <summary>
    /// 定义权限分组
    /// </summary>
    /// <param name="displayName">
    /// 分组显示名称
    /// 设置此值：使用FixedLocalizableString(displayName)
    /// 不设置此值：自动Key需要单独配置LocalizableString
    /// </param>
    public PermissionGroupAttribute(string displayName = null)
    {
        if (displayName.HasValue())
            DisplayName = new FixedLocalizableString(displayName);
    }

    /// <summary>
    /// 定义权限/组的类型
    /// </summary>
    public Type DeclaringType { get; set; }

    /// <summary>
    /// 权限的分组名称
    /// 如果不指定：
    /// CurrName=Type.Name去掉Permissions后缀
    /// 1，Group：CurrName
    /// 2，Permission：Group.Name.Parent.Name.CurrName
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 分组的本地化名称
    /// </summary>
    public ILocalizableString DisplayName { get; set; }

    /// <summary>
    /// 本地化名称LocalizableKey（如果不指定Key为：Permissions:CurrName/>）
    /// </summary>
    public string DisplayNameKey { get; set; }
}


﻿using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace Jy.Abp.Authorization;

[DependsOn(typeof(JyAbpCoreModule), typeof(AbpAuthorizationModule))]
public class JyAbpAuthorizationModule : AbpModule
{
    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var moduleAssemblies = context.ServiceProvider
            .GetRequiredService<IModuleContainer>()
            .Modules
            .Select(t => t.Assembly)
            .ToArray();
        //EnumHelper.RegisterAssemblies(moduleAssemblies);
        PermissionHelper.RegisterEnumPermissionTypes(moduleAssemblies);
        base.OnApplicationInitialization(context);
    }
}

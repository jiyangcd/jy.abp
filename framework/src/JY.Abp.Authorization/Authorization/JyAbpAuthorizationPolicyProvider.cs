using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Volo.Abp.Authorization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.DependencyInjection;

namespace Jy.Abp.Authorization;

[Dependency(ReplaceServices = true)]
public class JyAbpAuthorizationPolicyProvider : AbpAuthorizationPolicyProvider
{
    public JyAbpAuthorizationPolicyProvider(
        IOptions<AuthorizationOptions> options,
        IPermissionDefinitionManager permissionDefinitionManager)
        : base(options, permissionDefinitionManager)
    {

    }

    public override async Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
    {
        var permissions = PermissionHelper.GetFromCombination(policyName);
        if (permissions.Length > 1)
        {
            var policyBuilder = new AuthorizationPolicyBuilder(Array.Empty<string>());
            policyBuilder.Requirements.Add(new PermissionsRequirement(permissions, false));
            return policyBuilder.Build();
        }
        return await base.GetPolicyAsync(policyName);
    }
}


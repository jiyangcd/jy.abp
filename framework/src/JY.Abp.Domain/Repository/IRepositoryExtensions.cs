﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;

namespace Jy.Abp.Repository;

public static class IRepositoryExtensions
{
    /// <summary>
    /// 是否不存在指定条件的数据
    /// </summary>
    public static async Task<bool> NotExistsAsync<TEntity, TPrimaryKey>(
        this IRepository<TEntity, TPrimaryKey> repository,
        Expression<Func<TEntity, bool>> predicate)
        where TEntity : class, IEntity<TPrimaryKey>
    {
        return !await repository.AnyAsync(predicate);
    }
}

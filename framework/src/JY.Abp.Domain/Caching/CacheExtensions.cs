﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Volo.Abp.Caching;

namespace Jy.Abp.Caching;

public static class CacheExtensions
{
    public static async Task SetAsync<TCacheItem, TCacheKey>(this IDistributedCache<TCacheItem, TCacheKey> distributedCache,
        TCacheKey key,
        TCacheItem value,
        TimeSpan absoluteExpirationRelativeToNow)
        where TCacheItem : class
    {
        var options = new DistributedCacheEntryOptions();
        options.AbsoluteExpirationRelativeToNow = absoluteExpirationRelativeToNow;
        await distributedCache.SetAsync(key, value, options);
    }
}

﻿using System.Threading.Tasks;
using Jy.Abp.Ddd.Domain.Shared;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Auditing;
using Volo.Abp.Domain;
using Volo.Abp.Modularity;
using Volo.Abp.Settings;

namespace Jy.Abp.Ddd.Domain;

[DependsOn(
    typeof(JyAbpDomainSharedModule),

    typeof(AbpDddDomainModule),
    typeof(AbpAuditingModule),
    //typeof(JyAbpBlobStoringModule),
    typeof(AbpSettingsModule)
 )]
public class JyAbpDomainModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        //context.Services.RegisterTypedSettings();
        //context.Services.OnRegistered(TypedSettingRegistrar.RegisterIfNeeded);
    }
}

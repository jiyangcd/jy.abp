﻿using System;
using System.Threading.Tasks;
using Microsoft.Playwright;

namespace Jy.Abp.Playwright;

public class BrowserScopeItem : IAsyncDisposable
{
    private Lazy<Task<IBrowser>> _browser;

    public BrowserScopeItem(
        string browserType,
        BrowserTypeLaunchOptions options,
        Func<string, BrowserTypeLaunchOptions, Task<IBrowser>> browserFactory)
    {
        BrowserType = browserType;
        Options = options;
        this._browser = new Lazy<Task<IBrowser>>(() => browserFactory(browserType, options));
    }

    public string BrowserType { get; private set; }

    public BrowserTypeLaunchOptions Options { get; private set; }

    public async Task<IBrowser> GetBrowserAsync()
    {
        return await this._browser.Value;
    }

    public async ValueTask DisposeAsync()
    {
        await (await this._browser.Value).DisposeAsync();
    }
}

﻿using System;
using System.Threading.Tasks;
using Microsoft.Playwright;

namespace Jy.Abp.Playwright;

/// <summary>
/// Playwright相关对像提供者
/// </summary>
public interface IPlaywrightProvider: IAsyncDisposable
{
    /// <summary>
    /// 获取配置
    /// </summary>
    PlaywrightOptions Options { get; }

    /// <summary>
    /// 获取IPlaywright对像（请不要直接回收此对像）
    /// </summary>
    Task<IPlaywright> GetPlaywrightAsync();

    /// <summary>
    /// 切换当前Provider的browser对像
    /// 切换以后GetBrowserAsync会返回当前上下文中的对像
    /// 每次UseBrowser都会创建新的Browser对像
    /// 释放IAsyncDisposable时会清除此次创建的对像
    /// </summary>
    IAsyncDisposable UseBrowser(string browserType = null, BrowserTypeLaunchOptions options = null);

    /// <summary>
    /// 获取当前browser对像（请不要直接回收此对像）
    /// </summary>
    Task<IBrowser> GetBrowserAsync();

    /// <summary>
    /// 获取一个IBrowserContext对像（browser来源于GetBrowserAsync）
    /// </summary>
    Task<IBrowserContext> GetContextAsync(BrowserNewContextOptions options = null);

    /// <summary>
    /// 获取一个page对像（browser来源于GetBrowserAsync）
    /// </summary>
    Task<IPage> GetPageAsync(BrowserNewPageOptions options = null);
}
﻿using Volo.Abp.Modularity;
using Volo.Abp.Threading;

namespace Jy.Abp.Playwright;

[DependsOn(typeof(JyAbpCoreModule), typeof(AbpThreadingModule))]
public class JyAbpPlaywrightModule : AbpModule
{

}

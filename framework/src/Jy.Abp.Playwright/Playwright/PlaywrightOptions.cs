﻿using System;
using Microsoft.Playwright;

namespace Jy.Abp.Playwright;

public class PlaywrightOptions
{
    public static string Section = "Settings";

    /// <summary>
    /// 浏览器类型(chromium,firefox,webkit)
    /// </summary>
    public string DefaultBrowserType { get; set; }
        = BrowserType.Chromium;

    /// <summary>
    /// 默认浏览器Launch配置
    /// </summary>
    public BrowserTypeLaunchOptions DefaultLaunchOptions { get; set; }
        = new BrowserTypeLaunchOptions();

    /// <summary>
    /// 默认的浏览器上下文配置
    /// </summary>
    public BrowserNewContextOptions DefaultContextOptions { get; set; }
        = new BrowserNewContextOptions();

    /// <summary>
    /// 默认的页面配置
    /// </summary>
    public BrowserNewPageOptions DefaultPageOptions { get; set; }
        = new BrowserNewPageOptions();

    /// <summary>
    /// 配置默认BrowserNewContextOptions、DefaultBrowserType
    /// </summary>
    public void ConfigureLaunch(Action<BrowserTypeLaunchOptions> configure, string defaultBrowserType = BrowserType.Chromium)
    {
        this.DefaultBrowserType = defaultBrowserType;
        configure?.Invoke(DefaultLaunchOptions);
    }

    /// <summary>
    /// 配置默认BrowserNewContextOptions
    /// </summary>
    public void ConfigureContext(Action<BrowserNewContextOptions> configure)
    {
        configure?.Invoke(DefaultContextOptions);
    }

    /// <summary>
    /// 配置默认BrowserNewPageOptions
    /// </summary>
    public void ConfigurePage(Action<BrowserNewPageOptions> configure)
    {
        configure?.Invoke(DefaultPageOptions);
    }
}

﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Playwright;

public class BrowserScopeItem
{
    public string BrowserType { get; set; }

    public BrowserTypeLaunchOptions LaunchOptions { get; set; }

    public IBrowser Browser { get; set; }

    public async Task InitIfNotAsync(IPlaywright playwright)
    {
        this.Browser = await playwright[BrowserType].LaunchAsync();
    }
}

class Program
{
    private static AsyncLocal<BrowserScopeItem> _asyncLocal = new AsyncLocal<BrowserScopeItem>();

    static Lazy<Task<int>> _test = new Lazy<Task<int>>(async () =>
    {
        Console.WriteLine("lazy create.");
        return await CreateTest();
    });
    static async Task<int> CreateTest() 
    {
        Console.WriteLine("test create.");
        return await Task.FromResult(1);
    }
    static async Task Main(string[] args)
    {
        for (var i = 1; i <= 100; i++) 
        {
            Console.WriteLine(await _test.Value);
        }
    }
}

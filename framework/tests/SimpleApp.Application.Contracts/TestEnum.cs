﻿using System.ComponentModel;

namespace Jy.Abp.App.Application.Contracts;

public enum TestEnum
{
    [Description("A类")]
    A,
    [Description("B类")]
    B,
    /// <summary>
    /// C类
    /// </summary>
    C
}

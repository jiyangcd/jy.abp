﻿namespace Jy.Abp.App.Application.Contracts;

public class TestInput
{
    /// <summary>
    /// TestEnum
    /// </summary>
    public TestEnum TestEnum { get; set; }

    public string TestStr { get; set; }
}

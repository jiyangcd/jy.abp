﻿using System;
using Jy.Abp.Application;
using Jy.Abp.Authorization;
using Jy.Abp.Identity;
using JY.Abp.GeneralTree;
using Volo.Abp.Modularity;

namespace SimpleApp;

[DependsOn(
    typeof(SimpleAppDomainSharedModule),
    typeof(JyAbpAuthorizationModule),
    typeof(JyAbpApplicationContractsModule),
    typeof(JyAbpGeneralTreeApplicationContractsModule),
    typeof(JyAbpIdentityApplicationContractsModule))]
public class SimpleAppApplicationContractsModule : AbpModule
{

}

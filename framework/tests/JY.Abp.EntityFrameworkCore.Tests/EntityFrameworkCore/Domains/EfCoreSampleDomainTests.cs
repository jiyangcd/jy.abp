using JY.Abp.Ddd.Samples;
using Xunit;

namespace JY.Abp.Ddd.EntityFrameworkCore.Domains;

[Collection(DddTestConsts.CollectionDefinitionName)]
public class EfCoreSampleDomainTests : SampleDomainTests<DddEntityFrameworkCoreTestModule>
{

}

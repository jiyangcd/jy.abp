﻿using Xunit;

namespace JY.Abp.Ddd.EntityFrameworkCore;

[CollectionDefinition(DddTestConsts.CollectionDefinitionName)]
public class DddEntityFrameworkCoreCollection : ICollectionFixture<DddEntityFrameworkCoreFixture>
{

}

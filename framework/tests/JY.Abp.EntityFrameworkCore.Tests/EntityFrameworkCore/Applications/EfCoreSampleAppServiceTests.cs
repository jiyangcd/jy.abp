using JY.Abp.Ddd.Samples;
using Xunit;

namespace JY.Abp.Ddd.EntityFrameworkCore.Applications;

[Collection(DddTestConsts.CollectionDefinitionName)]
public class EfCoreSampleAppServiceTests : SampleAppServiceTests<DddEntityFrameworkCoreTestModule>
{

}

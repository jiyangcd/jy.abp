﻿using Jy.Abp.BlobStoring;
using Jy.Abp.Ddd.Domain;
using Volo.Abp.BlobStoring;
using Volo.Abp.BlobStoring.FileSystem;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace Jy.Abp.Ddd;

[DependsOn(
    typeof(JyAbpDomainModule),
    typeof(TestBaseModule),
    typeof(AbpBlobStoringFileSystemModule)
)]
public class DddDomainTestModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        this.Configure<AbpBlobStoringOptions>(options => {
            options.Containers.ConfigureDefault(cfg =>
            {
                cfg.UseFileSystem(fs =>
                {
                    fs.BasePath = "f://test";
                });
            });
        });
    }
}

﻿using System.Threading.Tasks;
using Volo.Abp.Modularity;
using Volo.Abp.Testing;

namespace Jy.Abp.Ddd;

/* Inherit from this class for your domain layer tests. */
public abstract class DddDomainTestBase<TStartupModule> : TestBase<TStartupModule>
    where TStartupModule : IAbpModule
{

}

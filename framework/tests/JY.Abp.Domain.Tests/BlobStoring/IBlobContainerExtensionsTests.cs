﻿using Xunit;
using Jy.Abp.BlobStoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jy.Abp.Ddd;
using Volo.Abp.BlobStoring;
using Shouldly;

namespace Jy.Abp.BlobStoring.Tests;

public class IBlobContainerExtensionsTests : DddDomainTestBase<DddDomainTestModule>
{
    private IBlobContainerFactory _factory;
    private IBlobContainer<DddDomainTestModule> _blobContainer;
    public IBlobContainerExtensionsTests()
    {
        this._factory = this.GetRequiredService<IBlobContainerFactory>();
        this._blobContainer = this.GetRequiredService<IBlobContainer<DddDomainTestModule>>();
    }

    [Fact()]
    public void GetNameTest()
    {
        var container = _factory.Create("Test");
        //container.GetName().ShouldBe("Test");
        //container.GetName().ShouldBe("Test");

        //var typedContainer = _factory.Create<DddDomainTestModule>();
       // _blobContainer.GetName().ShouldBe(typeof(DddDomainTestModule).FullName);
    }

    [Fact()]
    public void GetConfigurationTest()
    {
        var container = _factory.Create("Test");
        //container.GetConfiguration().ShouldNotBeNull();
    }
}
﻿using Jy.Abp.Ddd.Domain.Shared;
using Jy.Abp.GeneralTree;
using Jy.Abp.Localization;
using Volo.Abp.Identity;
using Volo.Abp.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Modularity;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace SimpleApp;

[DependsOn(
    typeof(JyAbpGeneralTreeDomainSharedModule),
    typeof(AbpIdentityDomainSharedModule))]
public class SimpleAppDomainSharedModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<SimpleAppDomainSharedModule>();
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Add<SimpleAppResource>(SimpleAppResource.DefaultCultureName)
                .AddBaseTypes(typeof(AbpValidationResource))
                .AddVirtualJson("/Localization/SimpleApp");

            //options.DefaultResourceType = typeof(AppResource);
        });

        Configure<AbpExceptionLocalizationOptions>(options =>
        {
            options.MapCodeNamespace("SimpleApp", SimpleAppResource.Type);
        });
    }
}

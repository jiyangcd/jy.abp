﻿using System;
using Volo.Abp.Localization;

namespace Jy.Abp.Localization;

[LocalizationResourceName("Jy.Abp")]
public class SimpleAppResource
{
    /// <summary>
    /// 默认语言
    /// </summary>
    public const string DefaultCultureName = "zh-Hans";

    /// <summary>
    /// 类型
    /// </summary>
    public static Type Type = typeof(SimpleAppResource);
}

﻿using Jy.Abp.Application;
using Jy.Abp.Authorization;
using Jy.Abp.Ddd.Authorization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Modularity;

namespace Jy.Abp.Ddd;

[DependsOn(
    typeof(JyAbpApplicationModule),
    typeof(DddDomainTestModule)
)]
public class DddApplicationTestModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        this.Configure<AbpPermissionOptions>(opts =>
        {
            opts.DefinitionProviders.Add<TypedPermissionDefinitionProvider>();
        });

        Configure<AbpExceptionLocalizationOptions>(options =>
        {
            options.MapCodeNamespace("Test", typeof(TestResource));
        });
    }
}

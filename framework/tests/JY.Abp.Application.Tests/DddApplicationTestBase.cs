﻿using Volo.Abp.Modularity;

namespace Jy.Abp.Ddd;

public abstract class DddApplicationTestBase<TStartupModule> : TestBase<TStartupModule>
    where TStartupModule : IAbpModule
{

}

﻿using Jy.Abp.Authorization;

namespace Jy.Abp.Ddd.Authorization;

[PermissionGroup]
public class TestPermissions
{
    [Permission("多语言测试")]
    public enum LanTest
    {
        Create,
        Update,
        Delete,
    }

    [Permission("邮件测试")]
    public enum EmailTest
    {
        Test
    }
}

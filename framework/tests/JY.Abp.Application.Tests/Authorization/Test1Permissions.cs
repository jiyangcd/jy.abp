﻿using Jy.Abp.Authorization;

namespace Test.Authorization;

[PermissionGroup]
public class Test1Permissions
{
    [Permission("多语言测试")]
    public enum P1
    {
        Create,
        Update,
        Delete,
    }

    [Permission("邮件测试")]
    public enum P2
    {
        Test
    }
}
﻿using Jy.Abp.Authorization;
using Shouldly;
using Test.Authorization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.Testing;
using Xunit;

namespace Jy.Abp.Ddd.Authorization;

public class PermissionHelperTests : AbpIntegratedTest<DddApplicationTestModule>
{
    private readonly IPermissionDefinitionManager _permissionDefinitionManager;
    public PermissionHelperTests()
    {
        _permissionDefinitionManager = GetRequiredService<IPermissionDefinitionManager>();
    }

    [Fact()]
    public void GetPermissionNameTest()
    {
        PermissionHelper.GetPermissionName<TestPermissions.EmailTest>()
            .ShouldBe("Test.EmailTest");

        //var group = PermissionHelper.GetGroup(typeof(TestPermissions));
        // group.Name.ShouldBe("Test");
        // group.DisplayName.ShouldBeNull();

        PermissionHelper.GetPermissionName(TestPermissions.EmailTest.Test)
            .ShouldBe("Test.EmailTest.Test");

        PermissionHelper.GetCombination(
            TestPermissions.LanTest.Create,
            TestPermissions.LanTest.Update)
            .ShouldBe("Test.LanTest.Create|Test.LanTest.Update");

        PermissionHelper.GetFromCombination("Test.LanTest.Create|Test.LanTest.Update")
            .ShouldNotBeEmpty();
    }

    [Fact()]
    public void GetGroupTest()
    {
        var group1 = PermissionHelper.GetGroup(typeof(TestPermissions));
        group1.Name.ShouldBe("Test");
        group1.DisplayName.ShouldBeNull();
        group1.DeclaringType = typeof(TestPermissions);

        var group2 = PermissionHelper.GetGroup(typeof(Test1Permissions));
        group2.Name.ShouldBe("Testp");
        group2.DisplayName.ShouldNotBeNull();
        group2.DeclaringType = typeof(Test1Permissions);
    }

    [Fact()]
    public void GetDefineTest()
    {
        var define = PermissionHelper.GetDefine(typeof(TestPermissions.LanTest));
        define.Name.ShouldBe("Test.LanTest");
        define.DisplayName.ShouldBeAssignableTo(typeof(FixedLocalizableString));
        define.DeclaringType = typeof(TestPermissions.LanTest);


    }
}

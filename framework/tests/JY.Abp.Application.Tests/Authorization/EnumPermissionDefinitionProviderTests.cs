﻿using Jy.Abp.Authorization;
using Jy.Abp.Ddd;
using Jy.Abp.Localization;
using Shouldly;
using Test.Authorization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.Testing;
using Xunit;

namespace Jy.Abp.Ddd.Authorization;

public class EnumPermissionDefinitionProviderTests : AbpIntegratedTest<DddApplicationTestModule>
{
    private readonly IPermissionDefinitionManager _permissionDefinitionManager;
    private readonly TypedPermissionDefinitionProvider _provider;
    public EnumPermissionDefinitionProviderTests()
    {
        _provider = GetRequiredService<TypedPermissionDefinitionProvider>();
        _permissionDefinitionManager = GetRequiredService<PermissionDefinitionManager>();
    }


    [Fact()]
    public void DefineTest()
    {
        //_provider.Define();
    }

    [Fact()]
    public void GetDisplayNameTest()
    {
        var group1 = PermissionHelper.GetGroup(typeof(TestPermissions));

        var displayName1 = _provider.GetDisplayName(group1);

        //((LocalizableString)displayName1)
        //     .ResourceType
        //     .ShouldBe(typeof(JyAbpResource));

        var group2 = PermissionHelper.GetGroup(typeof(Test1Permissions));

        var displayName2 = _provider.GetDisplayName(group2);

        ((LocalizableString)displayName2)
            .ResourceType
            .ShouldBe(typeof(TestResource));
    }
}
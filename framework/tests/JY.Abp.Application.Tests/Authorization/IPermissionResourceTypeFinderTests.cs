﻿using Jy.Abp.Ddd;
using Jy.Abp.Ddd.Authorization;
using Jy.Abp.Localization;
using Shouldly;
using Test.Authorization;
using Volo.Abp.Testing;
using Xunit;

namespace Jy.Abp.Authorization.Tests;

public class IPermissionResourceTypeFinderTests : AbpIntegratedTest<DddApplicationTestModule>
{
    private readonly IResourceTypeProvider _permissionResourceTypeFinder;
    public IPermissionResourceTypeFinderTests() 
    {
        this._permissionResourceTypeFinder = this.GetRequiredService<IResourceTypeProvider>();
    }
    
    [Fact()]
    public void GetResourceTypeTest()
    {
        //this._permissionResourceTypeFinder
        //    .GetResourceType(typeof(TestPermissions))
        //    .ShouldBe(typeof(JyAbpResource));

        this._permissionResourceTypeFinder
            .GetResourceType(typeof(Test1Permissions))
            .ShouldBe(typeof(TestResource));
    }
}
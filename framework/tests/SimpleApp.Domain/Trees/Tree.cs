﻿using Jy.Abp.GeneralTree;

namespace SimpleApp.Trees;

public class Tree : GeneralTreeWithGuidBase<Tree>
{
    public TreeType Type { get; set; }
}

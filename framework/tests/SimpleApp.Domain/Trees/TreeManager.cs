﻿using System;
using System.Linq.Expressions;
using Jy.Abp.GeneralTree;
using Jy.Abp.GeneralTree.Localization;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;

namespace SimpleApp.Trees;

public class TreeManager : GeneralTreeManager<Tree, Guid>
{
    public TreeManager(IGeneralTreeCodeGenerator generalTreeCodeGenerator,
                       IGeneralTreeRepository<Tree, Guid> generalTreeRepository,
                       IOptions<GeneralTreeOptions> generalTreeOptionsAccess,
                       IStringLocalizer<GeneralTreeResource> generalTreeStringLocalizer)
        : base(generalTreeCodeGenerator, generalTreeRepository, generalTreeOptionsAccess, generalTreeStringLocalizer)
    {
    }

    protected override Expression<Func<Tree, bool>> GetCheckSameExpression(Tree tree)
    {
        return t => t.Name == tree.Name && t.Type == tree.Type && t.Id != tree.Id && t.ParentId == tree.ParentId;
    }
}

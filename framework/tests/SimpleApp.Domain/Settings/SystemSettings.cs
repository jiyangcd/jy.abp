﻿using Jy.Abp.Settings;

namespace SimpleApp.Settings;

public class SystemSettings : TypedSettingsBase
{
    [TypedSettingDefinition("SimpleApp")]
    public virtual string Name { get; set; }

    [TypedSettingDefinition("jy abp 测试项目")]
    public virtual string Description { get; set; }

    [TypedSettingDefinition(SystemSettingsType.Test1)]
    public virtual SystemSettingsType Type { get; set; }

    [TypedSettingDefinition(true)]
    public virtual bool BoolSetting { get; set; }

    [TypedSettingDefinition(2.0)]
    public virtual decimal DecimalSetting { get; set; }

    [TypedSettingDefinition(1.0)]
    public virtual double DoubleSetting { get; set;}

    [TypedSettingDefinition(1)]
    public virtual int IntSetting { get; set; }

    public virtual SystemSettingsItem Item { get; set; } = new SystemSettingsItem() { 
         Id = 1,
         Name = "item1"
    };

    public virtual SystemSettingsItem Item1 { get; set; }

    //[TypedSettingDefinition(1)]
    public virtual SystemSettingsItem[] Items { get; set; }
        = new[] { new SystemSettingsItem()
        {  Id = 2,Name="item2"},
            new SystemSettingsItem()
        {  Id = 3,Name="item3"}};
}

public class SystemSettingsItem 
{
    public int Id { get; set; }

    public string Name { get; set; }
}

public enum SystemSettingsType 
{
    Test1,
    Test2,
    Test3
}

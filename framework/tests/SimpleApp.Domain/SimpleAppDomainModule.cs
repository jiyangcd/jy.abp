﻿using Jy.Abp.GeneralTree;
using Jy.Abp.SettingManagement;
using SimpleApp.Trees;
using Volo.Abp.Identity;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement.Identity;

namespace SimpleApp;

[DependsOn(
    typeof(SimpleAppDomainSharedModule),
    typeof(JyAbpSettingManagementDomainModule),
    typeof(AbpIdentityDomainModule),
    typeof(AbpPermissionManagementDomainIdentityModule)
    )]
public class SimpleAppDomainModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpLocalizationOptions>(options =>
        {
            options.Languages.Add(new LanguageInfo("en", "en", "English"));
            options.Languages.Add(new LanguageInfo("zh-Hans", "zh-Hans", "简体中文"));
        });
    }
}

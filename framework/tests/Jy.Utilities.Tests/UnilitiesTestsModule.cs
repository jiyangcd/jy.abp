﻿using Jy.Abp.Playwright;
using Volo.Abp.Modularity;

namespace Jy.Abp.Ddd;

[DependsOn(
    typeof(TestBaseModule),
    typeof(JyAbpPlaywrightModule)
    )]
public class UnilitiesTestsModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {

    }
}

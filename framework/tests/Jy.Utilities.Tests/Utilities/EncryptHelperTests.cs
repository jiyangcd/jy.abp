﻿using Xunit;
using Jy.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shouldly;

namespace Jy.Utilities.Tests;

public class EncryptHelperTests
{
    [Fact()]
    public void Md5Test()
    {
        EncryptHelper.Md5("Abcd1234").ShouldBe("325a2cc052914ceeb8c19016c091d2ac");
        EncryptHelper.Md5("Test").ShouldBe("0cbc6611f5540bd0809a388dc95a615b");
    }

    [Fact()]
    public void Sha256Test()
    {
        EncryptHelper.Sha256("Abcd1234").ShouldBe("3f21a8490cef2bfb60a9702e9d2ddb7a805c9bd1a263557dfd51a7d0e9dfa93e");
    }

    [Fact()]
    public void DesEncryptTest()
    {
        EncryptHelper.DesEncrypt("Abcd1234", "Abcd1234").ShouldBe("GRzwQw2MbfBprKSB2kpnwQ==");
    }

    [Fact()]
    public void DesDecryptTest()
    {
        EncryptHelper.DesDecrypt("GRzwQw2MbfBprKSB2kpnwQ==", "Abcd1234").ShouldBe("Abcd1234");
    }
}
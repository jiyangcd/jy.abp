﻿using Shouldly;
using Xunit;

namespace Jy.Utilities.Tests;

public class ConvertHelperTests
{
    [Fact()]
    public void ChangeTypeTest()
    {
        ConvertHelper.ChangeType(111, typeof(string)).ShouldBe("111");
        ConvertHelper.ChangeType("1.25", typeof(double)).ShouldBe(1.25);
        ConvertHelper.ChangeType("1.25", typeof(decimal)).ShouldBe(1.25);
        ConvertHelper.ChangeType("1", typeof(int)).ShouldBe(1);
        ConvertHelper.ChangeType(null, typeof(int?)).ShouldBe(null);
        ConvertHelper.ChangeType(null, typeof(TestEnum?)).ShouldBe(null);
        ConvertHelper.ChangeType("A", typeof(TestEnum?)).ShouldBe(TestEnum.A);
        ConvertHelper.ChangeType("A", typeof(TestEnum)).ShouldBe(TestEnum.A);

        Assert.Throws<FormatException>(() => ConvertHelper.ChangeType(null, typeof(double)));
        Assert.Throws<FormatException>(() => ConvertHelper.ChangeType("A", typeof(double)));
    }

    public enum TestEnum
    {
        A = 1
    }
}
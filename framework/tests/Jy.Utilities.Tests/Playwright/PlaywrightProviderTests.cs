﻿using Jy.Abp.Ddd;
using Microsoft.Playwright;
using Shouldly;
using Xunit;

namespace Jy.Abp.Playwright.Tests;

public class PlaywrightProviderTests : UnilitiesTestsBase
{
    private readonly IPlaywrightProvider _provider;

    public PlaywrightProviderTests()
    {
        this._provider = this.GetRequiredService<IPlaywrightProvider>();
    }

    [Fact()]
    public async Task GetPlaywrightTest()
    {
        var p1 = await _provider.GetPlaywrightAsync();
        var p2 = await _provider.GetPlaywrightAsync();

        Assert.Equal(p1, p2);
        await _provider.DisposeAsync();
    }

    [Fact()]
    public async Task GetBrowserAsyncTest()
    {
        var b1 = await this._provider.GetBrowserAsync();
        var b2 = await this._provider.GetBrowserAsync();
        //直接获取两个应该是同一个对像
        Assert.Equal(b1, b2);

        await using (this._provider.UseBrowser(BrowserType.Firefox))
        {
            var bb1 = await this._provider.GetBrowserAsync();
            var bb2 = await this._provider.GetBrowserAsync();
            Assert.Equal(BrowserType.Firefox, bb1.BrowserType.Name);
            //切换浏览器以后获取的对像是同一个
            Assert.Equal(bb1, bb2);
            //切换后的和原来不是同一个
            Assert.NotEqual(b1, bb1);
            await using (this._provider.UseBrowser())
            {
                var bbb1 = await this._provider.GetBrowserAsync();
                var bbb2 = await this._provider.GetBrowserAsync();
                //切换浏览器以后获取的对像是同一个
                Assert.Equal(bbb1, bbb2);
                //切换后的和原来不是同一个
                Assert.NotEqual(bb1, bbb1);
            }
        }
        var b3 = await this._provider.GetBrowserAsync();
        Assert.Equal(b1, b3);
    }

    private async Task ContextTaskAsync(string key = "default")
    {
        var keyxx = new Random(Guid.NewGuid().GetHashCode()).Next(1, 5);
        await using (var cache = await this._provider.GetCachedContextAsync($"key_{keyxx}"))
        {
            var page = await cache.NewPageAsync("https://www.baidu.com");
            await page.CloseAsync();
        }
    }

    [Fact()]
    public async Task GetContextAsyncTest()
    {
        await using var c1 = await this._provider.GetContextAsync();
        await using var c2 = await this._provider.GetContextAsync(new()
        {
            BaseURL = "aaa"
        });
        Assert.NotEqual(c1, c2);
        Assert.Equal(c1.Browser, c2.Browser);

        var tasks = new List<Task>();
        for (var i = 1; i <= 30; i++)
        {
            tasks.Add(ContextTaskAsync());
        }
        await Task.WhenAll([.. tasks]);
    }

    [Fact()]
    public async Task GetPageAsyncTest()
    {
        var b1 = await this._provider.GetPageAsync();
        var b2 = await this._provider.GetPageAsync(new()
        {
            Offline = false
        });

        Assert.NotEqual(b1.Context, b2.Context);
    }
}
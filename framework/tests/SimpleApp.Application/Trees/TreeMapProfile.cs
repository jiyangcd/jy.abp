﻿using AutoMapper;

namespace SimpleApp.Trees;

public class TreeMapProfile : Profile
{
    public TreeMapProfile()
    {
        CreateMap<Tree, TreeDto>();
        CreateMap<Tree, TreeListDto>();
        CreateMap<TreeEditDto, Tree>();
    }
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Jy.Abp.GeneralTree;
using Volo.Abp.Domain.Repositories;

namespace SimpleApp.Trees;

public class CategoryAppService : GeneralTreeAppServiceBase<Tree, Guid, TreeFilter, TreePagedListFilter, TreeEditDto, TreeListDto, TreeDto>
{
    private TreeType _treeType = TreeType.Category;

    public CategoryAppService(TreeManager treeManager, IRepository<Tree, Guid> treeRepository)
        : base(treeManager, treeRepository)
    {

    }

    protected override Tree Map(TreeEditDto dto)
    {
        var entity = base.Map(dto);
        entity.Type = _treeType;
        return entity;
    }

    protected override void Map(TreeEditDto dto, Tree entity)
    {
        base.Map(dto, entity);
        entity.Type = _treeType;
    }

    protected override async Task<IQueryable<Tree>> GetQueryAsync()
    {
        return (await base.GetQueryAsync())
                .Where(t=>t.Type == _treeType);
    }
}

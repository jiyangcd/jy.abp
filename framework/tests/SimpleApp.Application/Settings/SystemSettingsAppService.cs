﻿using Jy.Abp.SettingManagement;

namespace SimpleApp.Settings;

public class SystemSettingsAppService : GlobalSettingsAppServiceBase<SystemSettings>
{
    private SystemSettings _systemSettings;// { get; set; }

    public SystemSettingsAppService(
        ITypedSettingsManager manager,
        SystemSettings settings)
        : base(manager)
    {
        this._systemSettings = settings;
    }

    public SystemSettings GetTest1()
    {
        return _systemSettings;
    }
}

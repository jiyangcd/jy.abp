using Jy.Abp.GeneralTree;
using Jy.Abp.Identity;
using Jy.Abp.SettingManagement;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;

namespace SimpleApp;

[DependsOn(
    typeof(SimpleAppDomainModule),
    typeof(SimpleAppApplicationContractsModule),

    typeof(JyAbpSettingManagementApplicationModule),
    typeof(JyAbpIdentityApplicationModule),
    typeof(JyAbpGeneralTreeApplicationModule),
    typeof(AbpPermissionManagementApplicationModule)
    )]
public class SimpleAppApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddMaps<SimpleAppApplicationModule>();
        });
    }
}

﻿using System;
using System.Threading.Tasks;
using Jy.Abp.AspNetCore.Serilog;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using SimpleApp.Domain.Shared;

namespace AbpProjectTemplate.Web;

public class Program
{
    public async static Task<int> Main(string[] args)
    {
        try
        {
            Log.Information("Starting web host.");
            var builder = WebApplication.CreateBuilder(args);
            builder.Host
                .AddAppSettingsSecretsJson()
                .UseAutofac()
                .UseAbpSerilog();
                //.UseSerilog((context, services, configuration) =>
                //{
                //    configuration
                //     .ReadFrom.Configuration(context.Configuration)
                //     .ReadFrom.Services(services)
                //     .Enrich.FromLogContext();
                //});
            await builder.AddApplicationAsync<SimpleAppWebModule>();
            var app = builder.Build();
            await app.InitializeApplicationAsync();
            await app.RunAsync();
            return 0;
        }
        catch (Exception ex)
        {
            if (ex is HostAbortedException)
            {
                throw;
            }

            Log.Fatal(ex, "Host terminated unexpectedly!");
            return 1;
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }
}

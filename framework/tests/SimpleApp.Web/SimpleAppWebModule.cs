﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Castle.DynamicProxy;
using Jy.Abp.AspNetCore;
using Jy.Abp.AspNetCore.Serilog;
using Jy.Abp.AspNetCore.Swagger;
using Jy.Abp.Identity;
using Jy.Abp.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SimpleApp.EntityFrameworkCore;
using SimpleApp.HttpApi;
using SimpleApp.Settings;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Serilog;
using Volo.Abp.Autofac;
using Volo.Abp.BlobStoring;
using Volo.Abp.BlobStoring.FileSystem;
using Volo.Abp.DependencyInjection;
using Volo.Abp.DynamicProxy;
using Volo.Abp.Modularity;
using Volo.Abp.Settings;
using Volo.Abp.Swashbuckle;

namespace SimpleApp.Domain.Shared;

[DependsOn(
    typeof(SimpleAppHttpApiModule),
    typeof(SimpleAppApplicationModule),
    typeof(SimpleAppEntityFrameworkCoreModule),

    //typeof(JyAbpAspNetCoreModule),
    typeof(JyAbpAspNetCoreSerilogModule),
    //typeof(AbpAspNetCoreSerilogModule),
    typeof(AbpAutofacModule),
    typeof(AbpBlobStoringFileSystemModule)
    )]
public class SimpleAppWebModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        var environment = context.Services.GetHostingEnvironment();

        this.Configure<AbpBlobStoringOptions>(options =>
        {
            options.Containers.ConfigureDefault(cfg =>
            {
                cfg.UseFileSystem(fs =>
                {
                    var path = Path.Combine(environment.ContentRootPath, "App_Data");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    fs.BasePath = path;
                });
            });
        });

        this.Configure<AbpAspNetCoreMvcOptions>(options =>
        {
            options.ConventionalControllers.Create(typeof(SimpleAppApplicationModule).Assembly);
            //options.ConventionalControllers.Create(typeof(JyAbpIdentityApplicationModule).Assembly);
        });

        //context.Services.AddAbpSwaggerGen(options =>
        //{
        //    options.ConfigureDefault();
        //});

        context.Services.AddAbpSwaggerGenWithOidc(
            "http://localhost:44326/",
            ["GeneralTree"],
            flows: [AbpSwaggerOidcFlows.Password, AbpSwaggerOidcFlows.AuthorizationCode],
            setupAction: options =>
            {
                options.ConfigureDefault();
            });

        context.Services.AddAlwaysAllowAuthorization();

    }

    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();
        var env = context.GetEnvironment();

        // Configure the HTTP request pipeline.
        if (env.IsDevelopment())
        {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseRouting();
        app.UseAuthorization();
        app.UseConfiguredEndpoints();
        app.UseAbpSerilogEnrichers();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapGet("/", context =>
            {
                context.Response.Redirect("/swagger");
                return Task.CompletedTask;
            });
        });
        app.UseSwagger();
        app.UseAbpSwaggerUI(options =>
        {
            options.ConfigureDefault(context.GetApplicationName());
            options.OAuthClientId("SimpleApp.Web");
            //options.OAuthScopes("GeneralTree");
        });
    }
}

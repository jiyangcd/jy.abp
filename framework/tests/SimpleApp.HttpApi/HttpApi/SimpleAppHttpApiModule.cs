﻿using Jy.Abp.BlobStoring;
using Jy.Abp.Identity;
using Jy.Abp.Localization;
using Localization.Resources.AbpUi;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement.HttpApi;

namespace SimpleApp.HttpApi;

[DependsOn(
    typeof(SimpleAppApplicationContractsModule),
    typeof(JyAbpBlobStoringHttpApiModule),
    typeof(JyAbpIdentityHttpApiModule),
    typeof(AbpPermissionManagementHttpApiModule))]
public class SimpleAppHttpApiModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<SimpleAppResource>()
                .AddBaseTypes(
                    typeof(AbpUiResource)
                );
        });
    }
}

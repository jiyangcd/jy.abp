﻿using Jy.Abp.BlobStoring;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.BlobStoring;

namespace SimpleApp.HttpApi.Files;

[Route(RouteTemplete)]
public class FilesController : BlobControllerBase
{
    public const string RouteTemplete = "files";
    //public const string DefaultContainerName = DefaultContainer.Name;

    public FilesController(IBlobContainerFactory blobContainerFactory)
        : base(blobContainerFactory)
    {
    }
}

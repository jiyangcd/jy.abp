﻿using Jy.Abp.App.Application.Contracts;
using Jy.Abp.BlobStoring;
using Jy.Abp.Services;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Jy.Abp.App.HttpApi;

[ApiController]
[Route("/api/[controller]/[action]")]
//[SwaggerTag("Controller for todo")]
public class TestController : AbpController
{
    [HttpPost]
    [ServieGroup("Test1")]
    public IActionResult Index(TestInput input)
    {
        return Content("test");
    }

    [HttpGet]
    [ServieGroup("Test2")]
    public IActionResult Index1(TestEnum @enum)
    {
        return Content("test1");
    }


    [HttpPost]
    [ServieGroup("Test2")]
    public IActionResult Index2(BlobAutoDirType @enum)
    {
        return Content("test2");
    }

    [HttpPost]
    [ServieGroup("Test2")]
    public IActionResult Index3([FromForm] BlobUploadInput @enum)
    {
        return Content("test2");
    }
}

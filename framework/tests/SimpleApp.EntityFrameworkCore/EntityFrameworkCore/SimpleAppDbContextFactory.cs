﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace SimpleApp.EntityFrameworkCore;

/* This class is needed for EF Core console commands
 * (like Add-Migration and Update-Database commands) */
public class SimpleAppDbContextFactory : IDesignTimeDbContextFactory<SimpleAppDbContext>
{
    public SimpleAppDbContext CreateDbContext(string[] args)
    {
        SimpleAppEfCoreEntityExtensionMappings.Configure();

        var configuration = BuildConfiguration();

        var builder = new DbContextOptionsBuilder<SimpleAppDbContext>()
            .UseSqlServer(configuration.GetConnectionString("Default"));

        return new SimpleAppDbContext(builder.Options);
    }

    private static IConfigurationRoot BuildConfiguration()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../SimpleApp.Web/"))
            .AddJsonFile("appsettings.json", optional: false);

        return builder.Build();
    }
}

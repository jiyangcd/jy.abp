﻿using Microsoft.EntityFrameworkCore;
using SimpleApp.Trees;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement.EntityFrameworkCore;

namespace SimpleApp.EntityFrameworkCore;

[ConnectionStringName("Default")]
public class SimpleAppDbContext :
    AbpDbContext<SimpleAppDbContext>
{
    /* Add DbSet properties for your Aggregate Roots / Entities here. */

    

    public DbSet<Tree>  Trees { get; set; }

    public SimpleAppDbContext(DbContextOptions<SimpleAppDbContext> options)
        : base(options)
    {

    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
#if DEBUG
        optionsBuilder.EnableSensitiveDataLogging();
#endif
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        /* Include modules to your migration db context */

        builder.ConfigureSettingManagement();
        builder.ConfigureIdentity();
        builder.ConfigurePermissionManagement();
    }
}

﻿using Jy.Abp.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.SqlServer;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement.EntityFrameworkCore;

namespace SimpleApp.EntityFrameworkCore;

[DependsOn(
    typeof(SimpleAppDomainModule),
    typeof(AbpEntityFrameworkCoreSqlServerModule),
    typeof(AbpSettingManagementEntityFrameworkCoreModule),
    typeof(AbpIdentityEntityFrameworkCoreModule),
    typeof(JyAbpGeneralTreeEntityFrameworkCoreModule),
    typeof(AbpPermissionManagementEntityFrameworkCoreModule)
    )]
public class SimpleAppEntityFrameworkCoreModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        SimpleAppEfCoreEntityExtensionMappings.Configure();
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<SimpleAppDbContext>(options =>
        {
            /* Remove "includeAllEntities: true" to create
			 * default repositories only for aggregate roots */
            options.AddDefaultRepositories(includeAllEntities: true);
            options.AddGeneralTreeRepositories();
            //options.AddGeneralTreeRepository<Category, Guid>();
        });

        Configure<AbpDbContextOptions>(options =>
        {
            /* The main point to change your DBMS.
			 * See also AbpProjectTemplateMigrationsDbContextFactory for EF Core tooling. */
            options.UseSqlServer();
            options.PreConfigure<SettingManagementDbContext>(options =>
            {
#if DEBUG
                options.DbContextOptions.EnableSensitiveDataLogging();
#endif
            });
        });
    }
}

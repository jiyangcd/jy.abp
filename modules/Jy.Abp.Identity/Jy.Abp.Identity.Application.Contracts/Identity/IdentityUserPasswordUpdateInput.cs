﻿using System.ComponentModel.DataAnnotations;

namespace Jy.Abp.Identity;

public class IdentityUserPasswordUpdateInput
{
    [Required]
    public string NewPassword { get; set; }
}

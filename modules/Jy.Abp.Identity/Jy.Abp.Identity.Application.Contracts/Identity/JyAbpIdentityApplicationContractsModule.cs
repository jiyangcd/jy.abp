﻿using Jy.Abp.Application;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace Jy.Abp.Identity;

[DependsOn(
    typeof(JyAbpApplicationContractsModule),
    typeof(AbpIdentityApplicationContractsModule))]
public class JyAbpIdentityApplicationContractsModule : AbpModule
{

}

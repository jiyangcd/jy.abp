﻿using System;
using System.Threading.Tasks;

namespace Jy.Abp.Identity;

public interface IUserAppService 
{
    Task UpdatePasswordAsync(Guid id, IdentityUserPasswordUpdateInput input);
}

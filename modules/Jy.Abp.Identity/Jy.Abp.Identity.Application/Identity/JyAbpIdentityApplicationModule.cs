﻿using Jy.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace Jy.Abp.Identity;

[DependsOn(
    typeof(JyAbpApplicationModule),
    typeof(JyAbpIdentityApplicationContractsModule),

    typeof(AbpIdentityApplicationModule)
    )]
public class JyAbpIdentityApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        this.Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddMaps<JyAbpIdentityApplicationModule>();
        });
    }
}

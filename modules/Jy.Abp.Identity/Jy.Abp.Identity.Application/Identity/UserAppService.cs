﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Identity;

namespace Jy.Abp.Identity;

/// <summary>
/// 用户
/// </summary>
[Dependency(ReplaceServices = true)]
[ExposeServices(typeof(IdentityUserAppService), typeof(IUserAppService), IncludeDefaults = true, IncludeSelf = true)]
public class UserAppService : IdentityUserAppService, IUserAppService
{
    public UserAppService(
        IdentityUserManager userManager,
        IIdentityUserRepository userRepository,
        IIdentityRoleRepository roleRepository,
        IOptions<IdentityOptions> identityOptions,
        IPermissionChecker permissionChecker)
        : base(userManager, userRepository, roleRepository, identityOptions, permissionChecker)
    {

    }

    /// <summary>
    /// 修改密码
    /// </summary>
    [Authorize(IdentityPermissions.Users.Update)]
    public async Task UpdatePasswordAsync(Guid id, IdentityUserPasswordUpdateInput input)
    {
        await IdentityOptions.SetAsync();

        var user = await UserManager
            .FindByIdAsync(id.ToString());

        var r = await UserManager
             .RemovePasswordAsync(user);
        r.CheckErrors();
        await UserManager.AddPasswordAsync(user, input.NewPassword);

        r.CheckErrors();
    }
}

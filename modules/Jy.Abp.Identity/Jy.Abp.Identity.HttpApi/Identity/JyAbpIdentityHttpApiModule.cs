﻿using Volo.Abp.Identity;
using Volo.Abp.Modularity;

namespace Jy.Abp.Identity;

[DependsOn(
    typeof(JyAbpIdentityApplicationContractsModule),
    typeof(AbpIdentityHttpApiModule))]
public class JyAbpIdentityHttpApiModule : AbpModule
{

}

﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;

namespace Jy.Abp.Identity;

//[Dependency(ReplaceServices = true)]
//[ExposeServices(typeof(IdentityUserController), typeof(UserController))]
[RemoteService(true, Name = "AbpIdentity")]
[Area("identity")]
[ControllerName("User")]
[Route("api/identity/users")]
public class UserController : AbpController, IUserAppService
{
    protected IUserAppService _userAppService;

    public UserController(IUserAppService userAppService)
    {
        _userAppService = userAppService;
    }

    [HttpPut("{id}/password")]
    public async Task UpdatePasswordAsync(Guid id, IdentityUserPasswordUpdateInput input)
    {
        await this._userAppService.UpdatePasswordAsync(id, input);
    }
}

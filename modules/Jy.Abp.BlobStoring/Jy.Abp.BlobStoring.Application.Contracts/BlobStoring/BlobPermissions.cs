﻿using Jy.Abp.Authorization;

namespace Jy.Abp.BlobStoring;

[PermissionGroup]
public class BlobPermissions
{
    [Permission]
    public enum Manage
    {
        Upload,
        View,
        Delete
    }
}

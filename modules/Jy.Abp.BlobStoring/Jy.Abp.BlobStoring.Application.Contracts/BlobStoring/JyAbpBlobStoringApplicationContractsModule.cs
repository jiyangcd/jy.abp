﻿using Jy.Abp.Application;
using Jy.Abp.BlobStoring.Localization;
using Volo.Abp.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace Jy.Abp.BlobStoring;

[DependsOn(typeof(JyAbpApplicationContractsModule))]
public class JyAbpBlobStoringApplicationContractsModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<JyAbpBlobStoringApplicationContractsModule>("Jy.Abp");
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Add<JyAbpBlobStoringResource>("en")
                .AddVirtualJson("/BlobStoring/Localization");
        });

        Configure<AbpExceptionLocalizationOptions>(options =>
        {
            options.MapCodeNamespace("Jy.Abp.BlobStoring", typeof(JyAbpBlobStoringResource));
        });
    }
}

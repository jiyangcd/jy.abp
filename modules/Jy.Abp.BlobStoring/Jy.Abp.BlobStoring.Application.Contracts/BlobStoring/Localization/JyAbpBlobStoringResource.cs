﻿using Volo.Abp.Localization;

namespace Jy.Abp.BlobStoring.Localization;

[LocalizationResourceName("JyAbpBlobStoring")]
public class JyAbpBlobStoringResource
{

}

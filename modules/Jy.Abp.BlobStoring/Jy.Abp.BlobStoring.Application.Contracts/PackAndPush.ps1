Write-Host "开始打包.."
del ../../../nupkg/*.nupkg
dotnet pack --configuration Release --output ../../../nupkg	
Write-Host "请按下任意键推送包"
$null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")

$packages = Get-ChildItem -Path ../../../nupkg -Filter *.nupkg -Recurse
$nugetUrl = "https://api.nuget.org/v3/index.json"

foreach($package in $packages)
{
	Write-Host '开始发布：'$package.FullName
	dotnet nuget push $package.FullName --skip-duplicate -s $nugetUrl --api-key $env:NugetApiKey
}

Write-Host "Press any key to continue..."  
$null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
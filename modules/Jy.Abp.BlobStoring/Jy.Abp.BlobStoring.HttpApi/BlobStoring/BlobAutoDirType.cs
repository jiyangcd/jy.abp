﻿namespace Jy.Abp.BlobStoring;

public enum BlobAutoDirType
{
    /// <summary>
    /// 不生成目录
    /// </summary>
    None = 0,
    /// <summary>
    /// 按年
    /// </summary>
    Year = 1,
    /// <summary>
    /// 按月
    /// </summary>
    Month = 2,
    /// <summary>
    /// 按天
    /// </summary>
    Day = 4
}

﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Jy.Abp.BlobStoring;


public class BlobUploadInput
{
    /// <summary>
    /// 容器名称,默认值：default
    /// 一般用使用业务来区分
    /// </summary>
    public string ContainerName { get; set; }
    /// <summary>
    /// 自动目录类型，默认值：Month
    /// </summary>
    public BlobAutoDirType? AutoDirType { get; set; }
    /// <summary>
    /// 文件名，如果不指定自动生成，可不指定后缀名，会自动从文件读取
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// 文件
    /// </summary>
    [Required]
    public IFormFile File { get; set; }
}

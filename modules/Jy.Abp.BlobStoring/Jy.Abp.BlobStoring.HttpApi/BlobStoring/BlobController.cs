﻿using Jy.Abp.Application;
using Jy.Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.BlobStoring;

namespace Jy.Abp.BlobStoring;

[Route("blob")]
[AppAuthorize<BlobPermissions.Manage>]
public class BlobController : BlobControllerBase
{
    public BlobController(IBlobContainerFactory blobContainerFactory)
        : base(blobContainerFactory)
    {
        this.CreatePermissionName = PermissionHelper.GetPermissionName(BlobPermissions.Manage.Upload);
        this.GetPermissionName = PermissionHelper.GetPermissionName(BlobPermissions.Manage.View);
        this.DeletePermissionName = PermissionHelper.GetPermissionName(BlobPermissions.Manage.Delete);
    }
}

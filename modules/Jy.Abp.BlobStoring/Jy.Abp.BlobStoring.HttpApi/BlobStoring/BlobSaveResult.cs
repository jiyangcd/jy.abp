﻿namespace Jy.Abp.BlobStoring;

public class BlobSaveResult
{
    public string Name { get; set; }

    public string ContainerName { get; set; }

    public string Url { get; set; }

}

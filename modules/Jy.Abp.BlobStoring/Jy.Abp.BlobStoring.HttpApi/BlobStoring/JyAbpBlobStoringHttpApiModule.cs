using Jy.Abp.Application;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;

namespace Jy.Abp.BlobStoring;

[DependsOn(
    typeof(JyAbpBlobStoringModule),
    typeof(JyAbpBlobStoringApplicationContractsModule),

    typeof(AbpAspNetCoreMvcModule))]
public class JyAbpBlobStoringHttpApiModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {

    }
}
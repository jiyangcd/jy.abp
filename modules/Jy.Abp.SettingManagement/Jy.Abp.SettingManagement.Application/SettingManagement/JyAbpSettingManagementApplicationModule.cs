﻿using Jy.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.SettingManagement;

namespace Jy.Abp.SettingManagement;

[DependsOn(
    typeof(JyAbpSettingManagementDomainModule),
    typeof(JyAbpApplicationModule)
    //typeof(AbpSettingManagementApplicationModule))
    )]
public class JyAbpSettingManagementApplicationModule : AbpModule
{

}

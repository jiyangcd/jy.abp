﻿using System;
using System.Threading.Tasks;
using Jy.Abp.Auditing;
using Jy.Abp.Settings;

namespace Jy.Abp.SettingManagement;

public abstract class UserSettingsAppServiceBase<TTypedSettings> : TypedSettingsAppServiceBase
    where TTypedSettings : class, ITypedSettings
{
    protected UserSettingsAppServiceBase(ITypedSettingsManager manager) : base(manager)
    {
    }

    /// <summary>
    /// 获取配置
    /// </summary>
    public async Task<TTypedSettings> GetAsync(Guid userId)
    {
        await CheckPolicyAsync(GetPermission);
        return await _manager.GetForUserAsync<TTypedSettings>(userId);
    }

    /// <summary>
    /// 修改配置
    /// </summary>
    [AuditTag("修改")]
    public async Task UpdateAsync(Guid userId, TTypedSettings settings)
    {
        await CheckPolicyAsync(UpdatePermission);
        await _manager.SetForUserAsync(userId, settings);
    }
}

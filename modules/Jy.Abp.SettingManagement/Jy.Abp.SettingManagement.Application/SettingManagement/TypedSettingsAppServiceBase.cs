﻿using Jy.Abp.Application;

namespace Jy.Abp.SettingManagement;

public abstract class TypedSettingsAppServiceBase : AppService
{
    protected readonly ITypedSettingsManager _manager;

    protected TypedSettingsAppServiceBase(ITypedSettingsManager manager)
    {
        _manager = manager;
    }

    protected string GetPermission { get; set; }
    protected string UpdatePermission { get; set; }
}

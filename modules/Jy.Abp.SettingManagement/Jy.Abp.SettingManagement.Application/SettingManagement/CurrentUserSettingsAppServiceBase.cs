﻿using System.Threading.Tasks;
using Jy.Abp.Auditing;
using Jy.Abp.Settings;

namespace Jy.Abp.SettingManagement;

public abstract class CurrentUserSettingsAppServiceBase<TTypedSettings> : TypedSettingsAppServiceBase
    where TTypedSettings : class, ITypedSettings
{
    public CurrentUserSettingsAppServiceBase(ITypedSettingsManager manager) : base(manager)
    {
    }

    /// <summary>
    /// 获取配置
    /// </summary>
    public virtual async Task<TTypedSettings> GetAsync()
    {
        await CheckPolicyAsync(GetPermission);
        return await _manager.GetForCurrentUserAsync<TTypedSettings>();
    }

    /// <summary>
    /// 修改配置
    /// </summary>
    [AuditTag("修改")]
    public virtual async Task UpdateAsync(TTypedSettings settings)
    {
        await CheckPolicyAsync(UpdatePermission);
        await _manager.SetForCurrentUserAsync(settings);
    }
}

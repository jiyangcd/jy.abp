﻿using Jy.Abp.Ddd.Domain;
using Jy.Abp.Settings;
using Volo.Abp.Modularity;
using Volo.Abp.SettingManagement;

namespace Jy.Abp.SettingManagement;

[DependsOn(
    typeof(JyAbpDomainModule),
    typeof(JyAbpSettingsModule),

    typeof(AbpSettingManagementDomainModule)
    )]
public class JyAbpSettingManagementDomainModule : AbpModule { }

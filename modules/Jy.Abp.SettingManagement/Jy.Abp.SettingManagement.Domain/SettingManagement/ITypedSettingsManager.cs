﻿using System.Threading.Tasks;
using Jy.Abp.Settings;

namespace Jy.Abp.SettingManagement;

public interface ITypedSettingsManager
{
    Task<TSettings> GetAsync<TSettings>(string providerName, string providerKey, bool fallback = true) 
        where TSettings : ITypedSettings;
    Task SetAsync<TSettings>(TSettings settings, string providerName, string providerKey, bool forceToSet = false) 
        where TSettings : ITypedSettings;
}

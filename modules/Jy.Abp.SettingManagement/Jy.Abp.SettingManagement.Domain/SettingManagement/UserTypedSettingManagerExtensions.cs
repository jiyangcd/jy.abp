﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Jy.Abp.Settings;
using Volo.Abp.Settings;

namespace Jy.Abp.SettingManagement;

public static class UserTypedSettingManagerExtensions
{
    public static Task<TSettings> GetForUserAsync<TSettings>(this ITypedSettingsManager settingManager, Guid userId, bool fallback = true)
        where TSettings : ITypedSettings
    {
        return settingManager.GetAsync<TSettings>(UserSettingValueProvider.ProviderName, userId.ToString(), fallback);
    }

    public static Task<TSettings> GetForCurrentUserAsync<TSettings>(this ITypedSettingsManager settingManager, bool fallback = true)
        where TSettings : ITypedSettings
    {
        return settingManager.GetAsync<TSettings>(UserSettingValueProvider.ProviderName, null, fallback);
    }

    public static Task SetForUserAsync<TSettings>(this ITypedSettingsManager settingManager, Guid userId, [NotNull] TSettings settings, bool forceToSet = false)
        where TSettings : ITypedSettings
    {
        return settingManager.SetAsync(settings, UserSettingValueProvider.ProviderName, userId.ToString(), forceToSet);
    }

    public static Task SetForCurrentUserAsync<TSettings>(this ITypedSettingsManager settingManager, [NotNull] TSettings settings, bool forceToSet = false)
        where TSettings : ITypedSettings
    {
        return settingManager.SetAsync(settings, UserSettingValueProvider.ProviderName, null, forceToSet);
    }
}
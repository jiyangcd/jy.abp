﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Jy.Abp.Settings;
using Volo.Abp.Settings;

namespace Jy.Abp.SettingManagement;

public static class GlobalTypedSettingManagerExtensions
{
    public static Task<TSettings> GetGlobalAsync<TSettings>(this ITypedSettingsManager settingManager, bool fallback = true)
        where TSettings : ITypedSettings
    {
        return settingManager.GetAsync<TSettings>(GlobalSettingValueProvider.ProviderName, null, fallback);
    }

    public static Task SetGlobalAsync<TSettings>(this ITypedSettingsManager settingManager, [NotNull] TSettings settings)
        where TSettings : ITypedSettings
    {
        return settingManager.SetAsync(settings, GlobalSettingValueProvider.ProviderName, null);
    }
}

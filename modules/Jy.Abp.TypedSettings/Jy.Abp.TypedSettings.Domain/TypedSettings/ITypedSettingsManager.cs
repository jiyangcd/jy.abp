﻿using System;
using System.Threading.Tasks;
using Jy.Abp.Settings;

namespace Jy.Abp.TypedSettings;

public interface ITypedSettingsManager
{
    Task<TSettings> GetAsync<TSettings>() where TSettings : class, ITypedSettings;
    Task SetForTenantAsync<TSettings>(Guid tenantId, TSettings settings) where TSettings : ITypedSettings;
    Task SetForUserAsync<TSettings>(Guid userId, TSettings settings) where TSettings : ITypedSettings;
    Task SetGlobalAsync<TSettings>(TSettings settings) where TSettings : ITypedSettings;
}


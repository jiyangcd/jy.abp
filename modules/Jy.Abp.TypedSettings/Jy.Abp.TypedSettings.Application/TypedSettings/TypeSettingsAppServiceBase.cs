﻿using System.Threading.Tasks;
using Jy.Abp.Application;
using Jy.Abp.Auditing;
using Jy.Abp.Settings;

namespace Jy.Abp.TypedSettings;

public class TypedSettingsAppServiceBase<TTypedSettings> : AppService
    where TTypedSettings : class, ITypedSettings
{
    protected readonly ITypedSettingsManager _manager;
    protected string GetPermission { get; set; }
    protected string UpdatePermission { get; set; }

    public TypedSettingsAppServiceBase(ITypedSettingsManager manager)
    {
        //_settings = updater.GetAsync<TTypedSettings>();
        _manager = manager;
    }

    /// <summary>
    /// 获取配置
    /// </summary>
    public virtual async Task<TTypedSettings> GetAsync()
    {
        await CheckPolicyAsync(GetPermission);
        return await _manager.GetAsync<TTypedSettings>();
    }

    /// <summary>
    /// 修改配置
    /// </summary>
    [AuditTag("修改")]
    public virtual async Task UpdateAsync(TTypedSettings settings)
    {
        await CheckPolicyAsync(UpdatePermission);
        await _manager.SetGlobalAsync(settings);
    }
}

public abstract class UserTypedSettingsAppServiceBase<TTypedSettings> : TypedSettingsAppServiceBase<TTypedSettings>
    where TTypedSettings : class, ITypedSettings
{
    protected UserTypedSettingsAppServiceBase(ITypedSettingsManager manager)
        : base(manager)
    {
    }

    /// <summary>
    /// 修改配置
    /// </summary>
    [AuditTag("修改")]
    public override async Task UpdateAsync(TTypedSettings settings)
    {
        await CheckPolicyAsync(UpdatePermission);
        await _manager.SetForUserAsync(CurrentUserId, settings);
    }
}

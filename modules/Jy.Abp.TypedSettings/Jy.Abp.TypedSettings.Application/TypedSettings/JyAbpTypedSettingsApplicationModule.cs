﻿using Jy.Abp.Application;
using Volo.Abp.Modularity;

namespace Jy.Abp.TypedSettings;

[DependsOn(
    typeof(JyAbpTypedSettingsDomainModule),
    typeof(JyAbpApplicationModule))]
public class JyAbpTypedSettingsApplicationModule : AbpModule
{

}

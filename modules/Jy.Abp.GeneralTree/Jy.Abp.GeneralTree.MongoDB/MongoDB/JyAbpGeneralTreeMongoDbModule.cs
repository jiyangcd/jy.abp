﻿using Jy.Abp.GeneralTree;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace JY.Abp.GeneralTree.MongoDB;

[DependsOn(
    typeof(JyAbpGeneralTreeDomainModule),
    typeof(AbpMongoDbModule)
    )]
public class JyAbpGeneralTreeMongoDbModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //context.Services.AddTransient(typeof(IGeneralTreeRepository<,>), typeof(MongoGeneralTreeRepository<,,>));
    }
}

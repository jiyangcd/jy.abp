﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.Application;
using JY.Abp.GeneralTree;

namespace Jy.Abp.GeneralTree;

[DependsOn(
    typeof(JyAbpGeneralTreeDomainModule),
    typeof(JyAbpGeneralTreeApplicationContractsModule),
    typeof(AbpDddApplicationModule),
    typeof(AbpAutoMapperModule)
    )]
public class JyAbpGeneralTreeApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAutoMapperObjectMapper<JyAbpGeneralTreeApplicationModule>();
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddMaps<JyAbpGeneralTreeApplicationModule>(validate: true);
        });
    }
}

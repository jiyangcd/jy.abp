﻿using System.Collections.Generic;

namespace Jy.Abp.GeneralTree;

public class GeneralTreeDto<TDto, TKey> : GeneralTreeListDto<TKey>,
    IGeneralTreeDto<TDto, TKey>
    where TKey : struct
{
    public ICollection<TDto> Children { get; set; }
}

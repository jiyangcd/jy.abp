﻿using System;
using Volo.Abp.Auditing;

namespace Jy.Abp.GeneralTree;

public class GeneralTreeListDto<TKey> : GeneralTreeEditDto<TKey>, IHasCreationTime
    where TKey : struct
{
    public TKey Id { get; set; }

    public int Level { get; set; }

    public string Code { get; set; }

    public string FullName { get; set; }

    public DateTime CreationTime { get; set; }
}

﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Validation;

namespace Jy.Abp.GeneralTree;

public class GeneralTreeEditDto<TKey>
    where TKey : struct
{
    /// <summary>
    /// 父级ID，为null表示顶级
    /// </summary>
    public virtual TKey? ParentId { get; set; }
    /// <summary>
    /// 名称 
    /// </summary>
    [Required]
    [DynamicStringLength(typeof(GeneralTreeConsts), nameof(GeneralTreeConsts.MaxNameLength))]
    public string Name { get; set; }
    /// <summary>
    /// 描述
    /// </summary>
    [DynamicStringLength(typeof(GeneralTreeConsts), nameof(GeneralTreeConsts.MaxDescLength))]
    public virtual string Description { get; set; }
    /// <summary>
    /// 排序
    /// </summary>
    public virtual int Sort { get; set; }
}

﻿using System;

namespace Jy.Abp.GeneralTree;

public class GeneralTreeFilter<TKey>
    where TKey : struct
{
    public string ParentCode { get; set; }
}

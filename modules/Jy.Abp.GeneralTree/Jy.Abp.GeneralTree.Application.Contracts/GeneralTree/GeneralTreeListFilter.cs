﻿using Jy.Abp.Application;

namespace Jy.Abp.GeneralTree;

public class GeneralTreeListFilter<TKey> : KeywordsPagedAndSortedFilter
    where TKey : struct
{
    public string ParentCode { get; set; }
}

﻿using System;

namespace Jy.Abp.GeneralTree;

public class GeneralTreeWithGuidDto<TDto> : GeneralTreeDto<TDto, Guid>
    where TDto : GeneralTreeWithGuidDto<TDto>
{

}

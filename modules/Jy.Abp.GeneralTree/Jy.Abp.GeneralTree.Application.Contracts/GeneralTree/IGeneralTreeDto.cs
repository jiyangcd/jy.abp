﻿using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace Jy.Abp.GeneralTree;

public interface IGeneralTreeDto<GeneralTreeDto, TPrimaryKey> : IEntityDto<TPrimaryKey>
    where TPrimaryKey : struct
{
    TPrimaryKey? ParentId { get; set; }

    ICollection<GeneralTreeDto> Children { get; set; }
}
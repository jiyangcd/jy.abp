﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;
using Jy.Abp.GeneralTree;

namespace JY.Abp.GeneralTree;

[DependsOn(
    typeof(JyAbpGeneralTreeDomainSharedModule),
    typeof(AbpDddApplicationContractsModule),
    typeof(AbpAuthorizationModule)
    )]
public class JyAbpGeneralTreeApplicationContractsModule : AbpModule
{

}

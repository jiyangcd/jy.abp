﻿using Jy.Abp.GeneralTree;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Jy.Abp.EntityFrameworkCore;

[DependsOn(typeof(JyAbpGeneralTreeDomainModule), typeof(AbpEntityFrameworkCoreModule))]
public class JyAbpGeneralTreeEntityFrameworkCoreModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //context.Services.AddTransient(typeof(IGeneralTreeRepository<,>), typeof(EfCoreGeneralTreeRepository<,,>));
    }
}
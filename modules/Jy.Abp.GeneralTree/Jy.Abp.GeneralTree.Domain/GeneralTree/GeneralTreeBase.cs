﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Validation;

namespace Jy.Abp.GeneralTree;

public abstract class GeneralTreeBase<TTree, TPrimaryKey>
    : AuditedAggregateRoot<TPrimaryKey>
    , IGeneralTree<TTree, TPrimaryKey>
     where TPrimaryKey : struct
{
    public virtual int Level { get; set; }

    [Required]
    [DynamicStringLength(typeof(GeneralTreeConsts), nameof(GeneralTreeConsts.MaxCodeLength))]
    public virtual string Code { get; set; }

    [Required]
    [DynamicStringLength(typeof(GeneralTreeConsts), nameof(GeneralTreeConsts.MaxNameLength))]
    public virtual string Name { get; set; }

    [DynamicStringLength(typeof(GeneralTreeConsts), nameof(GeneralTreeConsts.MaxFullNameLength))]
    public virtual string FullName { get; set; }

    [DynamicStringLength(typeof(GeneralTreeConsts), nameof(GeneralTreeConsts.MaxDescLength))]
    public virtual string Description { get; set; }

    public virtual int Sort { get; set; }

    public virtual TPrimaryKey? ParentId { get; set; }

    [ForeignKey(nameof(ParentId))]
    public virtual TTree Parent { get; set; }
    public virtual ICollection<TTree> Children { get; set; }
}

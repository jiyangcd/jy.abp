﻿using System;
using System.Linq.Expressions;

namespace Jy.Abp.GeneralTree;

public class GeneralTreeOptions
{
    /// <summary>
    /// 名称和代码间隔符
    /// </summary>
    public string Hyphen { get; set; } = "-";
}
﻿using Jy.Abp.Ddd.Domain;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace Jy.Abp.GeneralTree;

[DependsOn(
    typeof(JyAbpDomainModule),
    typeof(JyAbpGeneralTreeDomainSharedModule)
)]
public class JyAbpGeneralTreeDomainModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddTransient<IGeneralTreeCodeGenerator, GeneralTreeCodeGenerator>();
        context.Services.AddTransient(typeof(IGeneralTreeManager<,>), typeof(GeneralTreeManager<,>));
    }
}

﻿namespace Jy.Abp.GeneralTree;

public class GeneralTreeCodeGeneratorOptions
{
    public int CodeLength { get; set; } = 6;
}
﻿using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace Jy.Abp.GeneralTree;

public interface IGeneralTree { }

public interface IGeneralTree<TTree, TPrimaryKey> : IEntity<TPrimaryKey>, IGeneralTree
    where TPrimaryKey : struct
{
    int Level { get; set; }

    string Code { get; set; }

    string Name { get; set; }

    string FullName { get; set; }

    TTree Parent { get; set; }

    TPrimaryKey? ParentId { get; set; }

    ICollection<TTree> Children { get; set; }
}
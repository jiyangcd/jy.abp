﻿using Volo.Abp.Localization;

namespace Jy.Abp.GeneralTree.Localization;

[LocalizationResourceName("GeneralTree")]
public class GeneralTreeResource
{

}

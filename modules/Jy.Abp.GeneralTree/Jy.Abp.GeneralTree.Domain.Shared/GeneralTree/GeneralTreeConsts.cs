﻿namespace Jy.Abp.GeneralTree;

public class GeneralTreeConsts
{
    public static int MaxDepth { get; set; } = 6;

    public static int CodeUnitLength { get; set; } = 5;

    public static int MaxCodeLength { get; set; } = MaxDepth * (CodeUnitLength + 1) - 1;

    public static int MaxNameLength { get; set; } = 128;

    public static int MaxFullNameLength { get; set; } = MaxNameLength * MaxDepth;

    public static int MaxDescLength { get; set; } = 1000;
}

﻿using Jy.Abp.Ddd.Domain.Shared;
using Jy.Abp.GeneralTree.Localization;
using Volo.Abp.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Modularity;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace Jy.Abp.GeneralTree;

[DependsOn(
    typeof(JyAbpDomainSharedModule)
)]
public class JyAbpGeneralTreeDomainSharedModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<JyAbpGeneralTreeDomainSharedModule>("Jy.Abp");
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Add<GeneralTreeResource>("en")
                .AddBaseTypes(typeof(AbpValidationResource))
                .AddVirtualJson("/GeneralTree/Localization");
        });

        Configure<AbpExceptionLocalizationOptions>(options =>
        {
            options.MapCodeNamespace("Jy.Abp.GeneralTree", typeof(GeneralTreeResource));
        });
    }
}
